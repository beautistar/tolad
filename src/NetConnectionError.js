import React, { Component } from 'react';
import {
    View,
    Image,
    Dimensions
} from 'react-native';

export default class NoInterNet extends Component {
    render() {
        return (
           <View style={{flex:1,backgroundColor:'#ffff'}}>
                <Image source={require('./assets/images/nonet.png')} style={{width:'100%',height:Dimensions.get('screen').height/1.2}}/>
           </View>
        )
    }
}