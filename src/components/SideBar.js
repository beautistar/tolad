import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, Dimensions, AsyncStorage, Modal, CameraRoll, Alert } from 'react-native'
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';
import { captureScreen } from "react-native-view-shot";
import Share from 'react-native-share';
import Api from '../components/api'

export default class WorkAreasDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      screenshot: '',
    }
  }
  _renderButton(title, onPress) {
    return (
      <TouchableOpacity onPress={onPress} style={styles.button}>
        <Text style={styles.buttonText}>{title}</Text>
      </TouchableOpacity>
    )
  }

  _logout() {
    AsyncStorage.clear()
    Actions.reset('SignUp')
  }

  _share() {
    this.props.closeDrawer()
    setTimeout(() =>
      captureScreen({
        format: "jpg",
        quality: 0.8
      }).then(
        uri => {
          console.log("Image saved to", uri)
          if (uri) {
            this.setState({ modalVisible: true, screenshot: uri })
            CameraRoll.saveToCameraRoll(uri, 'photo')
            setTimeout(() => this.setState({ modalVisible: false }, () => this._shareScreenshot()), 4000)
          }
        },
        error => console.error("Oops, snapshot failed", error)
      ), 1500)
  }

  _shareScreenshot() {
    setTimeout(() =>
      Share.open({
        url: this.state.screenshot,
        type: 'image/jpg'
      }), 1000)
  }

  _renderScreenshot() {
    return (
      <Modal visible={this.state.modalVisible} transparent>
        <View style={{ flex: 1, padding: 20, backgroundColor: "rgba(0,0,0,0.5)" }}>
          <Image source={{ uri: this.state.screenshot }} style={{ borderWidth: 1, flex: 1 }} />
        </View>
      </Modal>
    )
  }

  _deleteProfileData() {
    var apiData = new FormData()
    apiData.append('walk_id', this.props.data.id)
    Api.post('/walk/delete_walk', apiData)
      .then((result) => {
        if (result.result_code == 200) {
          Actions.Home()
        }
      })
  }

  _deleteWalk() {
    this.props.closeDrawer()
    Alert.alert(
      '',
      'Are you sure you want to delete this walk?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this._deleteProfileData() },
      ],
      { cancelable: false }
    )
  }



  render() {
    let { user } = this.state
    console.log("walk id in side bar", this.props.data)
    return (
      <View style={styles.container}>
        <View style={{ width: '100%', padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 20 }}>
          <Text style={{ color: 'white', fontSize: 18, marginLeft: 10, fontFamily: 'Lato-Regular' }}>MENU</Text>
          <TouchableOpacity onPress={this.props.closeDrawer}>
            <Icon name='close' type='MaterialIcons' style={{ color: 'white', fontSize: 22 }} />
          </TouchableOpacity>
        </View>
        {this.props.details ?
          <View>
            <View style={{ width: '100%', marginTop: 20 }}>
              {this._renderButton('START THIS WALK', () => { this.props.closeDrawer(), Actions.Map({ id: this.props.data.id }) })}
              {this._renderButton('DELETE THIS PROFILE', () => this._deleteWalk())}
              {this._renderButton('SHARE THIS WALK', () => this._share())}
            </View>
            <View style={{ width: '100%', marginTop: 20 }}>
              <Text style={{ color: '#fff', fontSize: 18, letterSpacing: 5, marginLeft: 20, fontWeight: 'bold', fontFamily: 'lato-Bold', paddingLeft: 10 }}>---</Text>
            </View>
          </View> : null
        }
        <View style={{ width: '100%', marginTop: 20 }}>
          {this._renderButton('PROFILE', () => { Actions.PersonalProfile(), this.props.closeDrawer() })}
          {this._renderButton('SETTINGS', () => { Actions.Settings(), this.props.closeDrawer() })}
          {this._renderButton('PRIVACY', () => { Actions.Privacy(), this.props.closeDrawer() })}
          {this._renderButton('HELP', () => { Actions.Help(), this.props.closeDrawer() })}
          {this._renderButton('CONTACT', () => { Actions.Contact(), this.props.closeDrawer() })}
          {this.props.details ? null : this._renderButton('LOGOUT', () => this._logout())}
        </View>
        {this._renderScreenshot()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(96,84,78,0.9)',
  },
  button: {
    padding: 10,
    marginLeft: 20
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
    fontFamily: 'Lato-Bold'
  }
});

