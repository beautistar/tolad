import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, StatusBar,Alert,AsyncStorage } from 'react-native';
import { Header, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';


export default class AppHeader extends Component {
    render() {
        return (
            <View style={{ zIndex: 1 }}>
                <Header style={{ width: '100%', backgroundColor: 'white' }} transparent >
                    {this.props.back ?
                        <TouchableOpacity onPress={() => {this.props.data ? this.props.data() : this.props.backToHome ? Actions.reset('Home') : Actions.pop() }} style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='ios-arrow-back' type="Ionicons" style={{ color: '#7d716c', fontWeight: '100' }} />
                        </TouchableOpacity>
                        :
                        <View style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
                            {this.props.profile ? <Image source={this.props.profile ? { uri: this.props.profile } : require('../assets/images/defaultprofile.png')} style={{ height: 50, width: 50, borderRadius: 25, left: 10, top: Platform.OS == 'android' ? 15 : 5, position: 'absolute', zIndex: 1 }} /> : null}
                        </View>}
                    <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center' }}>
                        {this.props.title ? <Text style={{ fontSize: 18, color: '#7d716c', fontWeight: 'bold', fontFamily: 'Lato-Bold' }}>{this.props.title.toUpperCase()}</Text> : <Image source={require('../assets/images/headerImage.png')} style={{ height: 14, width: 150 }} />}
                    </View>
                   
                    {this.props.menu ? <TouchableOpacity onPress={this.props.openDrawer} style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='ios-menu' type='Ionicons' />
                    </TouchableOpacity> : 
                    this.props.rightButton?<TouchableOpacity onPress={()=>Actions.Help()} style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
                       <Text style={{color:'#a60150',fontSize:15,fontWeight:'bold'}}>{this.props.rightButton}</Text>
                    </TouchableOpacity>:<View style={{ width: '10%' }}></View>}
                </Header>
                <StatusBar backgroundColor='white' barStyle='dark-content' />
            </View>
        );
    }
}

