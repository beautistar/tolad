
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Animated,
  SafeAreaView,
} from 'react-native';
export default class MessageBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height:new Animated.Value(0),
      opacity:new Animated.Value(0),
    }
   
  }
  _animateMessage(){  
      Animated.parallel([
        Animated.timing(this.state.height,{toValue:40,duration:500}),
        Animated.timing(this.state.opacity,{toValue:1,duration:500})
    ]).start();
    setTimeout(()=>{
      Animated.parallel([
        Animated.timing(this.state.height,{toValue:0,duration:500}),
        Animated.timing(this.state.opacity,{toValue:0,duration:500})
      ]).start();
    },2000)
    
  }
  render() {
    return (
        <SafeAreaView style={{flex:1,width:'100%'}}>
          <Animated.View style={[styles.bar,{height:this.state.height,opacity:this.state.opacity,backgroundColor:this.props.color ?this.props.color:'red' }]}>
              <Text style={styles.message}>{this.props.message}</Text>
          </Animated.View>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  bar:{
    backgroundColor:'red',
    width:"100%",
    alignItems:'center',
    position:'absolute',
    bottom:0,
    zIndex:1,
    justifyContent:'center'
  },
  message:{
    color:'white',
    fontSize:16,
    fontFamily:'Lato-Regular',
    textAlign:'center'
  }
});
