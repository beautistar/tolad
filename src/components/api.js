import { Alert } from 'react-native'; 
class Api {
  static headers() {
    return {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    }
  }
  static get(route) {
    return this.tolad(route, 'GET');
  }
  static getWithParams(route,param) {
    return this.tolad(route, params, 'GET',)
    //return this.tolad(route, null,token, 'GET');
  }

  static put(route, params) {
    return this.tolad(route, params,'PUT')
  }

  static post(route, params) {
    return this.tolad(route, params, 'POST')
  }

  static delete(route, params) {
    return this.tolad(route, params,'DELETE')
  }
  static tolad(route, params,verb,) {
    const host = 'http://18.196.178.198/api'
    //const host = 'http://ec2-18-222-223-24.us-east-2.compute.amazonaws.com'
   
    
    const url = `${host}${route}`
    console.log(url)
    let options = Object.assign({ method: verb }, params ? { body: params } : null );
    options.headers = Api.headers()
    //console.log('---------Data inside api-----',params)
    //console.log('-------header called-------',Api.headers(token))
    return fetch(url, options).then( resp => {
      //console.log('-----my Response-----',resp)
      let json = resp.json();
    //console.log('-----inside api-----',json)
    //console.log('-----inside api-----',resp)
      if (resp.ok) {
        //console.log('-----inside api-----',resp)
        return json;
          //console.log('-----inside api-----',resp)
      }
      return json.then(err => {throw err});

    }).then( json => json)
    .catch(e=>{
      //console.log('Error',e)
      return e
      // Alert.alert(
      //   'Erorr',
      //   'Server not reponding OR check your internet connection',
      //   [
      //     {text: 'Try Again', onPress: () => console.log('OK Pressed')},
      //   ],
      //   { cancelable: false }
      // )
    });
  }
}
export default Api
