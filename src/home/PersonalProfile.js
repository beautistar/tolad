import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    ScrollView,
    KeyboardAvoidingView,
    Dimensions,
    StatusBar,
    Image,
    AsyncStorage,
    Alert,
} from 'react-native';
import Header from '../components/Header'
import { Icon } from 'native-base'
import ImagePicker from 'react-native-image-crop-picker';
import { Actions } from 'react-native-router-flux';
import MessageBar from '../components/messageBar'
import Api from '../components/api'

export default class PersonalProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            email: '',
            profile: '',
            arrowClick: 0,
            password: '',
            userID: ''
        }
    }

    _profile() {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            this.setState({ profile: image })
        });
    }

    componentWillMount() {
        AsyncStorage.getItem('userData')
            .then((userData) => {
                var user = JSON.parse(userData)
                this.setState({ username: user.username, email: user.email, profile: user.photo_url, userID: user.id })
            })

        AsyncStorage.getItem('password')
            .then((password) => {
                var passwordDetails = JSON.parse(password)
                this.setState({ password: passwordDetails })
            })

    }

    _saveProfile() {
        var apiData = new FormData()
        apiData.append("user_id", this.state.userID)
        apiData.append("username", this.state.username)
        apiData.append("email", this.state.email)
        apiData.append("password", this.state.password)
        apiData.append("photo_url", this.state.profile ? { uri: this.state.profile.path || this.state.profile, name: Date.now() + '.jpg', type: "image/jpeg" } : "")
        Api.post("/user/edit_profile", apiData)
            .then((result) => {
                if (result.result_code == 200) {
                    this.setState({ message: "Successfully Edited", color: 'green' }, () => this.messageBar._animateMessage())
                }
                else {
                    this.setState({ message: result.message, color: 'red' }, () => this.messageBar._animateMessage())
                }
            })
    }

    _deleteAccount() {
        var apiData = new FormData()
        apiData.append("user_id", this.state.userID)
        Api.post("/user/delete_profile", apiData)
            .then((result) => {
                if (result.result_code == 200) {
                    Alert.alert(
                        '',
                        'Are you sure you want to delete account?',
                        [
                            { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                            {
                                text: 'Yes', onPress: () => {
                                    AsyncStorage.clear()
                                    Actions.reset('SignUp')
                                }
                            },
                        ],
                        { cancelable: false }
                    )
                }
                else {
                    this.setState({ message: result.message, color: 'red' }, () => this.messageBar._animateMessage())
                }
            })
    }

    render() {
        return (
            <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? "padding" : null} style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: '#fff' }}>
                    <Header back={true} />
                    <StatusBar backgroundColor='white' barStyle='dark-content' />
                    <View style={{ alignItems: 'center', flex: 1, width: '100%' }}>
                        <ScrollView bounces={false}>

                            <View style={{ marginTop: 25, width: Dimensions.get('screen').width / 1.2, flexDirection: 'row', alignSelf: 'center' }}>
                                <Text style={{ fontSize: 18, fontFamily: 'Lato-Bold' }}>PROFILE SETTINGS </Text>
                            </View>
                            <View style={{ marginVertical: 20, width: Dimensions.get('screen').width / 1.2, alignSelf: 'center' }}>
                                <View style={styles.profilePictureView}>
                                    <Image source={{ uri: this.state.profile.path || this.state.profile }} style={{ height: Dimensions.get('window').width / 2.5, width: Dimensions.get('window').width / 2.5, borderRadius: Dimensions.get('window').width / 5 }} />
                                    <TouchableOpacity onPress={() => this._profile()} style={styles.interactView}>
                                        <View style={styles.plusIconView}>
                                            <Icon name='edit' type='MaterialIcons' style={styles.icon} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={{ width: Dimensions.get('screen').width, }}>
                                <View style={styles.txtInput}>
                                    <View style={{ width: '20%', marginLeft: 40, justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 14, fontFamily: 'Lato-Regular', fontStyle: 'italic' }}>Username: </Text>
                                    </View>
                                    <View style={{ width: '50%', marginLeft: 10, justifyContent: 'center' }}>
                                        <TextInput onChangeText={(text) => this.setState({ username: text })} onBlur={() => this.setState({ arrowClick: 0 })} editable={this.state.arrowClick == 1 ? true : false} style={{ fontSize: 15, fontFamily: 'Lato-Regular' }}>{this.state.username}</TextInput>
                                    </View>
                                    <TouchableOpacity onPress={() => this.setState({ arrowClick: 1 })} style={{ width: '10%', justifyContent: 'center', alignItems: 'flex-end' }}>
                                        <Icon name='arrow-right' type='SimpleLineIcons' style={styles.rightIcon} />
                                    </TouchableOpacity>
                                </View>

                                <View style={[styles.txtInput, { marginTop: 5 }]}>
                                    <View style={{ width: '20%', marginLeft: 40, justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 15, fontFamily: 'Lato-Regular', fontStyle: 'italic' }}>Email: </Text>
                                    </View>
                                    <View style={{ width: '50%', marginLeft: 10, justifyContent: 'center' }}>
                                        <TextInput onChangeText={(text) => this.setState({ email: text })} onBlur={() => this.setState({ arrowClick: 0 })} editable={this.state.arrowClick == 2 ? true : false} style={{ fontSize: 15, fontFamily: 'Lato-Regular' }}>{this.state.email}</TextInput>
                                    </View>
                                    <TouchableOpacity onPress={() => this.setState({ arrowClick: 2 })} style={{ width: '10%', justifyContent: 'center', alignItems: 'flex-end' }}>
                                        <Icon name='arrow-right' type='SimpleLineIcons' style={styles.rightIcon} />
                                    </TouchableOpacity>
                                </View>

                                <View style={[styles.txtInput, { marginTop: 5 }]}>
                                    <View style={{ width: '20%', marginLeft: 40, justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 15, fontFamily: 'Lato-Regular', fontStyle: 'italic' }}>Password: </Text>
                                    </View>
                                    <View style={{ width: '50%', marginLeft: 10, justifyContent: 'center' }}>
                                        <TextInput onChangeText={(text) => this.setState({ password: text })} onBlur={() => this.setState({ arrowClick: 0 })} editable={this.state.arrowClick == 3 ? true : false} secureTextEntry={true} style={{ fontSize: 15, fontFamily: 'Lato-Regular' }}>{this.state.password}</TextInput>
                                    </View>
                                    <TouchableOpacity onPress={() => this.setState({ arrowClick: 3 })} style={{ width: '10%', justifyContent: 'center', alignItems: 'flex-end' }}>
                                        <Icon name='arrow-right' type='SimpleLineIcons' style={styles.rightIcon} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>

                        <MessageBar ref={(ref) => this.messageBar = ref} message={this.state.message} color={this.state.color} />
                    </View>
                    <View style={{ width: Dimensions.get('screen').width, justifyContent: 'space-evenly', flexDirection: 'row', paddingVertical: 20 }}>
                        <TouchableOpacity onPress={() => this._saveProfile()} style={{ width: '25%', height: 50, backgroundColor: '#b7ca4a', borderColor: '#b7ca4a', justifyContent: 'center' }}>
                            <Text style={{ textAlign: 'center', fontSize: 15, fontFamily: 'Lato-Bold', color: '#fff' }}>SAVE</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this._deleteAccount()} style={{ width: '50%', height: 50, backgroundColor: '#e0e0e0', borderColor: '#e0e0e0', justifyContent: 'center' }}>
                            <Text style={{ textAlign: 'center', fontSize: 15, fontFamily: 'Lato-Bold', color: '#fff' }}>DELETE ACCOUNT</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    interactSideView: {
        marginTop: -10,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        position: 'absolute'
    },
    interactView: {
        position: 'absolute',
        bottom: 0,
        width: '90%',
        alignItems: 'flex-end'
    },
    plusIconSideView: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#EEEEEE',
        elevation: 20
    },
    icon: {
        color: '#746764',
        fontSize: 20
    },
    rightIcon: {
        color: '#746764',
        fontSize: 20
    },
    profilePictureView: {
        width: Dimensions.get('window').width / 2.5,
        height: Dimensions.get('window').width / 2.5,
        borderRadius: Dimensions.get('window').width / 5,
        backgroundColor: '#FAFAFA',
    },
    plusIconView: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        backgroundColor: '#eceaeb',
        borderWidth: 0.5,
        borderColor: '#eceaeb',
        elevation: 20
    },
    txtInput: {
        height: 45,
        backgroundColor: '#eceaeb',
        borderWidth: 0.5,
        borderColor: '#eceaeb',
        width: Dimensions.get('screen').width,
        justifyContent: 'center',
        flexDirection: 'row'
    },
})