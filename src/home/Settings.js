import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    Keyboard,
    TouchableOpacity,
    Picker,
    TextInput,
    AsyncStorage,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Header from '../components/Header'

export default class Settings extends Component {

    constructor(props) {
        super(props)
        this.state = {
            timevalue: '10',
            time: false,
            radius: false,
            radiusvalue: '50'
        }
    }
    componentWillMount() {
        AsyncStorage.getItem('time')
            .then((time) => {
                if (time) {
                    var time = JSON.parse(time / 1000)
                    this.setState({ timevalue: time.toString() })
                }
            })
        AsyncStorage.getItem('distance')
            .then((distance) => {
                if (distance) {
                    this.setState({ radiusvalue: distance })
                }
            })
    }

    _save() {
        AsyncStorage.setItem('time', JSON.stringify(parseInt(this.state.timevalue) * 1000))
        AsyncStorage.setItem('distance', JSON.stringify(parseInt(this.state.radiusvalue)))
        Actions.pop()
    }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'rgb(236,234,235)' }}>
                <Header back={true} />
                <StatusBar backgroundColor='white' barStyle='dark-content' />
                <View style={{ width: '100%', alignItems: 'center', flex: 1 }}>
                    <View style={{ marginVertical: 30 }}>
                        <View style={{ width: '70%', alignItems: 'flex-start', marginTop: 30 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold', fontFamily: 'Lato-Bold' }}>TIME </Text>
                                <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular' }}>in seconds</Text>
                            </View>
                            <TextInput
                                style={[styles.txtInput, { marginTop: 10 }]}
                                placeholder='Time covered'
                                onFocus={() => { this.setState({ time: true, radius: false }, () => Keyboard.dismiss()) }}
                                value={this.state.timevalue}
                            />
                            {this.state.time ?
                                <Picker
                                    selectedValue={this.state.timevalue}
                                    style={{ width: 150 }}
                                    onValueChange={(itemValue, itemIndex) =>
                                        this.setState({ timevalue: itemValue, time: false })
                                    }>
                                    <Picker.Item label="3 seconds" value="3" />
                                    <Picker.Item label="5 seconds" value="5" />
                                    <Picker.Item label="8 seconds" value="8" />
                                    <Picker.Item label="10 seconds" value="10" />
                                    <Picker.Item label="15 seconds" value="15" />
                                    <Picker.Item label="20 seconds" value="20" />
                                </Picker>
                                : null
                            }
                        </View>

                        <View style={{ width: '70%', alignItems: 'flex-start', marginTop: 30 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold', fontFamily: 'Lato-Bold' }}>RADIUS </Text>
                                <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular' }}>in meters</Text>
                            </View>
                            <TextInput
                                style={[styles.txtInput, { marginTop: 10 }]}
                                placeholder='Distance covered'
                                onFocus={() => { this.setState({ time: false, radius: true }, () => Keyboard.dismiss()) }}
                                value={this.state.radiusvalue}
                            />
                            {this.state.radius ?
                                <Picker
                                    selectedValue={this.state.radiusvalue}
                                    style={{ width: 150 }}
                                    onValueChange={(itemValue, itemIndex) =>
                                        this.setState({ radiusvalue: itemValue, radius: false })
                                    }>
                                    <Picker.Item label="10 meter" value="10" />
                                    <Picker.Item label="25 meter" value="25" />
                                    <Picker.Item label="50 meter" value="50" />
                                    <Picker.Item label="75 meter" value="75" />
                                    <Picker.Item label="100 meter" value="100" />
                                    <Picker.Item label="150 meter" value="150" />
                                    <Picker.Item label="200 meter" value="200" />
                                </Picker> : null
                            }
                        </View>

                    </View>
                </View>
                <View style={{ bottom: 10, position: 'absolute', width: '100%', flexDirection: "row", justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', width: '90%', justifyContent: 'space-around' }}>
                        <TouchableOpacity onPress={() => this._save()} style={{ width: '75%', backgroundColor: '#ac014f', alignItems: 'center', padding: 14, borderRadius: 30, flexDirection: 'row', justifyContent: 'space-around', paddingHorizontal: 10 }}>
                            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 17, fontFamily: 'Lato-Bold' }}>SAVE AND CONTINUE</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    txtInput: {
        paddingLeft: 15,
        height: 50,
        backgroundColor: '#F4F4F4',
        borderWidth: 0.5,
        borderColor: '#F4F4F4',
        fontSize: 15,
        width: 150,
        fontFamily: 'Lato-Regular'
    },
})