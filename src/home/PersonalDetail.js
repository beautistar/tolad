import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    FlatList,
    TouchableOpacity,
    Image,
    SafeAreaView,
    Dimensions,
    ImageBackground,
    ScrollView,
    ActivityIndicator,
    PermissionsAndroid,
    StatusBar
} from 'react-native';
import { Header, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import moment from 'moment'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Api from '../components/api'
import Geolocation from 'react-native-geolocation-service';
var _ = require('lodash');

var bottom = Platform.select({
    ios: Math.round(Dimensions.get('window').height / 1.25),
    android: Math.round(Dimensions.get('window').height / 1.28)
})
var latLong
var location = []
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
export default class PersonalDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            image: '',
            backgrounds: '',
            indicator: true,
            location_list: [],
            data: [],
            walkPath: []
        }
    }
    componentWillMount() {
        if (Platform.OS == 'android') {
            PermissionsAndroid.request('android.permission.ACCESS_FINE_LOCATION')
                .then((res) => {
                    if (res == 'granted') {
                        Geolocation.getCurrentPosition((position) => {
                            var coords = { latitude: position.coords.latitude, longitude: position.coords.longitude }
                            this.setState({
                                latlong: {
                                    latitude: position.coords.latitude,
                                    longitude: position.coords.longitude,
                                }
                            })

                        },
                            (error) => console.log('Location Error', error),
                            {
                                enableHighAccuracy: true,
                                timeout: 20000,
                                maximumAge: 1000
                            }
                        )
                    }
                })
                .catch(err => console.log('Permission Error', err))
        }
        else {
            Geolocation.getCurrentPosition((position) => {
                var coords = { latitude: position.coords.latitude, longitude: position.coords.longitude }

                this.setState({
                    latlong: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    }
                })

            },
                (error) => console.log('Location Error', error),
                {
                    enableHighAccuracy: true,
                    timeout: 20000,
                    maximumAge: 1000
                }
            )
        }

        var apiData = new FormData()
        apiData.append('walk_id', this.props.data.id)
        Api.post('/walk/get_location', apiData)
            .then((result) => {
                var walk_number = function (d) {
                    return d.walk_number;
                }
                var latlong = function (d) {
                    return d.map(item => {
                        return {
                            latitude: Number(item.latitude),
                            longitude: Number(item.longitude)
                        };
                    })
                }
                var groupToSummary = function (group) {
                    return latlong(group)
                }
                var location_list = _(result.location_list)
                    .groupBy(walk_number)
                    .map(groupToSummary)
                    .value();

                lastlatlog = location_list.length > 0 ?
                    location_list[location_list.length - 1][location_list[location_list.length - 1].length - 1] : this.state.latlong
                if (result.result_code == 200) {
                    this.setState({ location_list: location_list, latlong: lastlatlog })
                }
            })
        this.setState({ image: this.props.profile, backgrounds: this.props.backgrounds, data: this.props.data, walkPath: this.state.location_list, latlong: this.state.latlong })
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({ indicator: false })
        }, 3000)
    }

    render() {
        if (this.state.indicator) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator color="#746761" size='large' />
                </View>
            )
        }
        else {
            return (
                <View style={styles.container}>
                    <Header style={{ width: '100%', backgroundColor: 'white' }} transparent >
                        <TouchableOpacity onPress={() => this.props.backNav()} style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='ios-arrow-back' type="Ionicons" style={{ color: '#7d716c', fontWeight: '100' }} />
                        </TouchableOpacity>
                        <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../assets/images/headerImage.png')} style={{ height: 14, width: 150 }} />
                        </View>
                        <TouchableOpacity onPress={this.props.openDrawer} style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='ios-menu' type='Ionicons' />
                        </TouchableOpacity>
                    </Header>
                    <StatusBar backgroundColor='white' barStyle='dark-content' />
                    <ImageBackground source={{ uri: this.state.data.background_url }} style={styles.backgroundImage} >
                        <View style={styles.secondaryView}>
                            <TouchableOpacity style={{ flex: 1 }} onPress={() => { Actions.WalkView({ data: this.state.data }) }}>
                                <View style={[styles.outerImgView]}>
                                    <TouchableOpacity onPress={() => Actions.ProfileImage({ fullImage: this.state.data.profile_url, title: this.state.data.deceased_name })} style={styles.profileImgView}>
                                        <Image source={{ uri: this.state.data.profile_url }} style={styles.image} />
                                    </TouchableOpacity>
                                    <View style={styles.interactView}>
                                        <Text style={[styles.dateTxt, { justifyContent: 'flex-start' }]}>
                                            {moment(this.state.data.birth_date).format('DD MMMM YYYY')}

                                        </Text>
                                        <View style={styles.roundMapView}>
                                            <TouchableOpacity onPress={() => Actions.Map({ _name: this.state.data.deceased_name, id: this.state.data.id, location: this.state.location_list })}>
                                                <MapView
                                                    style={{
                                                        height: width / 2.5,
                                                        width: width / 2.5,
                                                        borderRadius: width / 5,
                                                    }}
                                                    region={{
                                                        latitude: this.state.latlong ? this.state.latlong.latitude : 37.78825,
                                                        longitude: this.state.latlong ? this.state.latlong.longitude : -122.4324,
                                                        latitudeDelta: 0.025,
                                                        longitudeDelta: 0.0121,
                                                    }} />
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={[styles.dateTxt, { justifyContent: 'flex-end', }]}>
                                            {moment(this.state.data.passing_date).format('DD MMMM YYYY')}
                                        </Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <SafeAreaView style={styles.bottomView}>
                                <ScrollView showsVerticalScrollIndicator={true} contentContainerStyle={{ backgroundColor: 'white', paddingBottom: '25%' }}>
                                    <Text style={styles.bottomText}>
                                        {this.state.data.about_person}
                                    </Text>
                                </ScrollView>
                            </SafeAreaView>
                        </View>
                    </ImageBackground>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(236,234,235)'
    },
    backgroundImage: {
        width: '100%',
        height: '100%',
        alignItems: 'center'
    },
    secondaryView: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 30,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.2)'
    },
    outerImgView: {
        width: '100%',
        alignItems: 'center',
        top: Math.round(Dimensions.get('window').height / 15)
    },
    profileImgView: {
        backgroundColor: '#f4f4f4',
        height: height / 2,
        width: width / 1.1,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 25
    },
    image: {
        width: '100%',
        height: height / 2
    },
    interactView: {
        width: width,
        paddingTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        bottom: 20
    },
    dateTxt: {
        textAlign: 'center',
        width: 75,
        fontSize: 14,
        color: '#fff',
        fontWeight: '400',
        fontFamily: 'Lato-Regular',
        flexWrap: 'wrap',
    },
    roundMapView: {
        backgroundColor: '#ffffff',
        height: width / 2.5,
        width: width / 2.5,
        borderRadius: width / 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -80,
        elevation: 1,
        overflow: 'hidden'
    },
    bottomView: {
        width: '100%',
        height: height / 4,
        position: 'absolute',
        bottom: 5,
        backgroundColor: '#fff'
    },
    bottomText: {
        fontSize: 15,
        paddingHorizontal: 15,
        paddingVertical: 10,
        fontWeight: '400',
        lineHeight: 20,
        fontFamily: 'Lato-Regular'
    }
});