import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    Dimensions
} from 'react-native';
import Header from '../components/Header'
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
export default class ProfileImage extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Header back={true}/>
                <ImageBackground source={{uri:this.props.fullImage}} style={styles.backgroundImage} >
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(236,234,235)',
    },
    backgroundImage: {
        alignItems: 'center',
        height:height,
        width:width
    },
});