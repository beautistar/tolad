import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    StatusBar,
    Dimensions,
    ScrollView
} from 'react-native';

import Header from '../components/Header'
import { WebView } from 'react-native-webview';
export default class Help extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'rgb(236,234,235)' }}>
                <Header back={true} />
                <StatusBar backgroundColor='white' barStyle='dark-content' />
                <View style={{ alignItems: 'center', flex: 1 }}>
                    <ScrollView ref={(c) => { this.scroll = c }} onScroll={(e) => console.log(e.nativeEvent.contentOffset.x, e.nativeEvent.contentOffset.y)} showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: 20 }}>

                        <View style={{ marginTop: 30, width: Dimensions.get('screen').width / 1.2, alignSelf: 'center' }}>
                            <Text style={{ fontSize: 18, fontFamily: 'Lato-Bold' }}>HELP </Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Bold', paddingTop: 15 }}>How to use the TOLAD App?</Text>
                        </View>

                        <View style={{ justifyContent: 'center',borderWidth:1, height:Dimensions.get('screen').width/2, width: Dimensions.get('screen').width,marginVertical:15 }}>
                            <WebView
                                source={{ uri:'https://www.youtube.com/embed/nSncjYoT6sA'}}
                                javaScriptEnabled={true}
                                domStorageEnabled={true}   
                            />
                        </View>
                        
                        <View style={{ width: Platform.OS == 'android' ? Dimensions.get('screen').width / 1.3 : Dimensions.get('screen').width / 1.2, alignSelf: 'center' }}>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular', }}>Step 1: Open up Tolad</Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular', }}>Step 2: more information</Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular', }}>Step 1: Enjoy your walk</Text>
                        </View>

                        <View style={{ marginTop: 30, width: Dimensions.get('screen').width / 1.2, alignSelf: 'center' }}>
                            <Text style={{ fontSize: 17, fontFamily: 'Lato-Bold' }}>FREQUENTLY ASKED QUESTIONS </Text>
                        </View>

                        <View style={{ marginVertical: 15, width: Dimensions.get('screen').width / 1.2, alignSelf: 'center' }}>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Bold',paddingBottom:10 }}>Can you scatter ash everywhere with the TOLAD?</Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular',paddingBottom:10}}>
                             If no rules are laid down in the local regulation, you can, in principle, scatter ash everywhere.
Permission is always required from the owner of the land.
Never on children's playgrounds or on paved roads and / or paths.
                            </Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Bold',paddingBottom:10 }}>Can you reuse the TOLAD?</Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular',paddingBottom:10}}>
                            The TOLAD has been developed for single use.
                            </Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Bold',paddingBottom:10 }}>What should I do with the TOLAD after the walk?</Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular',paddingBottom:10}}>
                            You can dispose of the TOLAD in the normal way.
                            </Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Bold',paddingBottom:10 }}>Is the TOLAD Recyclable?</Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular',paddingBottom:10}}>
                            You can dispose of the TOLAD in the normal way.
                            </Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Bold',paddingBottom:10 }}>How much ash goes in the TOLAD?</Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular',paddingBottom:10}}>
                            approx. 3.5 liters or 120 ounces.
                            </Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Bold',paddingBottom:10 }}>How long can you walk with a full TOLAD</Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular',paddingBottom:10}}>
                            This depends on how often you put the TOLAD on the ground and press lightly or heavily.
                            </Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Bold',paddingBottom:10 }}>What is the distance that you walk with a full TOLAD</Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular',paddingBottom:10}}>
                            This depends on how often you put the TOLAD on the ground and press lightly or heavily
                            </Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Bold',paddingBottom:10 }}>You can use the TOLAD in all weather conditions</Text>
                            <Text style={{ fontSize: 13, fontFamily: 'Lato-Regular',paddingBottom:10}}>
                            Yes
                            </Text>
                        </View>
                        
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
})