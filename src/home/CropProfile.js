import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ImageEditor,
    Image,
    CameraRoll,
    FlatList,
    ImageBackground,
    Dimensions,
    Slider,
    PermissionsAndroid,
    TouchableOpacity
} from 'react-native';
import { Icon } from 'native-base'
export default class CropProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            images: [],
            selected: '',
            croped: '',
            crop: Dimensions.get('window').width,
        }
    }
    componentDidMount() {
        if (Platform.OS == 'android') {
            PermissionsAndroid.request('android.permission.READ_EXTERNAL_STORAGE')
                .then((res) => {
                    this._cameraRoll()
                })
        } else {
            this._cameraRoll()
        }

    }
    _cameraRoll() {
        CameraRoll.getPhotos({
            first: 50,
            assetType: 'All',
        })
            .then((item) => {
                this.setState({ images: item.edges, selected: item.edges[0].node.image.uri })
                this._edit()
            })
            .catch((e) => console.log('Error', e))
    }
    _edit() {
        ImageEditor.cropImage(
            this.state.selected,
            { offset: { x: 65, y: 70 }, size: { height: 200, width: 170 } },
            (data) => { console.log('croped image Data', data), this.setState({ croped: data }) },
            (e) => console.log('error', e))
    }
    render() {
        var height = this.state.crop
        return (
            <View style={styles.container}>
                <View style={{ height: Dimensions.get('window').height / 2, overflow: 'hidden' }}>
                    <ImageBackground source={{ uri: this.state.selected }} style={{ height: height, width: this.state.crop, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ height: 250, width: 250, borderRadius: 125, borderWidth: 2, borderColor: 'white', top: 50, position: 'absolute' }} />
                    </ImageBackground>
                </View>
                <View style={{ width: '100%', alignItems: 'center', padding: 10 }}>
                    <Slider style={{ width: '80%' }} onValueChange={(value) => this.setState({ crop: Math.round(value) }, () => this._edit())} maximumValue={800} minimumValue={Dimensions.get('window').width + 20} value={450} thumbTintColor={"#f26c2a"} minimumTrackTintColor={"#f26c2a"} />
                </View>
                <Image source={{ uri: this.state.croped }} style={{ height: 200, width: 200 }} />
                <View style={styles.btnView}>
                    <View>
                        <TouchableOpacity>
                            <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: '#a60150', alignItems: 'center', justifyContent: 'center' }}>
                                <Icon name='trash' type='FontAwesome5' style={{ color: '#fff', fontSize: 18 }} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity>
                            <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: '#4d6a26', alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontFamily: 'Lato-Bold', color: '#fff', fontSize: 15 }}>SAVE</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    btnView: {
        position: 'absolute',
        bottom: 20,
        justifyContent: 'space-evenly',
        width: 180,
        alignSelf: 'flex-start',
        flexDirection: 'row',
    },
    bottomBtn: {
        height: 50,
        width: 50
    },
});
