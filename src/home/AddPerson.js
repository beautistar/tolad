import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  Modal,
  Dimensions,
  StatusBar,
  Image,
  AsyncStorage,
  ImageBackground,
  DatePickerAndroid,
  DatePickerIOS,
  Keyboard,
  ActivityIndicator,
  Alert,
  BackHandler
} from 'react-native';
import Header from '../components/Header'
import { Icon } from 'native-base'
import ImagePicker from 'react-native-image-crop-picker';
import { Actions } from 'react-native-router-flux';
import moment from 'moment'
import MessageBar from '../components/messageBar'
import Api from '../components/api'
import DatePicker from 'react-native-datepicker'
import { isIphoneX } from 'react-native-iphone-x-helper'

var today = new Date()
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0');
var yyyy = today.getFullYear();
var currentDate = mm + '/' + dd + '/' + yyyy;
export default class AddPerson extends Component {
  constructor(props) {
    super(props)
    this.state = {
      profile: '',
      name: '',
      dob: '',
      dop: '',
      about: '',
      modalVisible: false,
      mapmodalVisible: this.props.startWalk ? this.props.startWalk : false,
      backgroundImage: null,
      user: {},
      dateOfBirth: false,
      dateOfPass: false,
      walkId: '',
      indicator: this.props.startWalk ? false : true,
      focus: false,
      loading: true,
      saveAlert: false,
      saveClick: false
    }
    this._addBackground = this._addBackground.bind(this)

  }
  _profile() {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      // console.log(image);
      this.setState({ profile: image, saveAlert: true })
      //Actions.CropProfile()
    });
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  setMapModalVisible(visible) {
    this.setState({ mapmodalVisible: visible })
  }
  _addBackground(background) {
    this.setState({ backgroundImage: background, saveAlert: true })
  }

  componentWillMount() {
    var user = this.props.personData != undefined ? this.props.personData : null
    AsyncStorage.getItem('userData')
      .then((userData) => {
        this.setState({ user: JSON.parse(userData) })
      })
    if (user != null || user != undefined) {
      this.setState({
        name: user.deceased_name,
        dob: moment(user.birth_date, 'MM/DD/YYYY'),
        dop: moment(user.passing_date, 'MM/DD/YYYY'),
        about: user.about_person,
        profile: user.profile_url ? { path: user.profile_url } : "",
        backgroundImage: user.background_url ? { background_url: user.background_url } : "",
        walkId: user.id
      })
    }

  }
  componentDidMount() {
    this._backHandle()
    setTimeout(() => {
      this.setState({ indicator: false })
    }, 3000)
  }

  _add() {
    this.setState({ loading: false })
    if (this.state.name && this.state.dob && this.state.dop) {
      var DateDOB = moment(this.state.dob).format('MM/DD/YYYY')
      var DateDOP = moment(this.state.dop).format('MM/DD/YYYY')

      var apiData = new FormData()
      apiData.append("user_id", this.state.user.id)
      apiData.append("deceased_name", this.state.name)
      apiData.append("birth_date", DateDOB)
      apiData.append("passing_date", DateDOP)
      apiData.append("about_person", this.state.about)
      apiData.append("profile_image", this.state.profile ? { uri: this.state.profile.path, name: Date.now() + '.jpg', type: "image/jpeg" } : "")
      if (this.state.backgroundImage) {
        console.log("insid3e")
        if (this.state.backgroundImage.id) {
          apiData.append("bg_image_id", this.state.backgroundImage.id)
          apiData.append("background_image", "")
        }
        else {
          console.log('IMage')
          apiData.append("bg_image_id", "0")
          apiData.append("background_image", this.state.backgroundImage ? { uri: this.state.backgroundImage.background_url, name: 'backgroundImage' + Date.now() + '.jpg', type: "image/jpeg" } : "")
        }
      } else {
        console.log('IMage')
        apiData.append("bg_image_id", "0")
        apiData.append("background_image", this.state.backgroundImage ? { uri: this.state.backgroundImage.background_url, name: 'backgroundImage' + Date.now() + '.jpg', type: "image/jpeg" } : "")
      }

      if (this.props.personData) {
        apiData.append("walk_id", this.props.personData.id)
        Api.post("/walk/edit_walk", apiData)
          .then((result) => {
            if (result.result_code == 200) {
              this.setState({ message: "Successfully Edited", color: 'green', loading: true, saveClick: true }, () => this.messageBar._animateMessage())
              this.props.walk ? this.props.walk() : null
            }
            else {
              this.setState({ message: result.message, color: 'red' }, () => this.messageBar._animateMessage())
            }
          })
      }
      else {
        Api.post("/walk/create_walk", apiData)
          .then((result) => {
            if (result.result_code == 200) {
              this.setState({ walkId: result.walk_id })
              this.setState({ message: "Created Successfully", color: 'green', loading: true }, () => this.messageBar._animateMessage())
              this.props.walk ? this.props.walk() : null
              setTimeout(() => {
                Actions.Home()
              }, 2500)
            }
            else {
              this.setState({ message: result.message, color: 'red' }, () => this.messageBar._animateMessage())
            }
          })
      }
    }
    else {
      this.setState({ message: "Please fill all the details", color: 'red' }, () => this.messageBar._animateMessage())
    }
  }

  _backHandle() {
    this.backHandle = BackHandler.addEventListener('backPress', () => {
      if (Actions.currentScene == 'AddPerson' && this.state.saveAlert == true && this.state.saveClick == false) {
        Alert.alert(
          'Attention',
          'You have unsaved changes to the profile, do you want to save or discard the changes?',
          [
            { text: 'Discard', onPress: () => Actions.pop(), style: 'cancel' },
            {
              text: 'Save', onPress: () => {
                this._add(),
                  this.setState({ saveAlert: false })
                setTimeout(() => {
                  Actions.pop()
                }, 3000)
              }
            },
          ],
          { cancelable: false }
        )
      }
      else {
        Actions.pop()
      }
      return true
    })
  }

  componentWillUnmount() {
    this.backHandle.remove()
  }

  _headerBackpress() {
    if (this.state.saveAlert == true && this.state.saveClick == false) {
      Alert.alert(
        'Attention',
        'You have unsaved changes to the profile, do you want to save or discard the changes?',
        [
          { text: 'Discard', onPress: () => Actions.pop(), style: 'cancel' },
          {
            text: 'Save', onPress: () => {
              this._add(),
                this.setState({ saveAlert: false })
              setTimeout(() => {
                Actions.pop()
              }, 3000)
            }
          },
        ],
        { cancelable: false }
      )
    }
    else {
      Actions.pop()
    }
  }

  _savedAlert() {
    if (this.state.saveAlert == true && this.state.saveClick == false) {
      Alert.alert(
        'Attention',
        'You have unsaved changes to the profile, do you want to save or discard the changes?',
        [
          { text: 'Discard', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          {
            text: 'Save', onPress: () => {
              this._add(),
                this.setState({ saveAlert: false })
              setTimeout(() => {
                this.setMapModalVisible(!this.state.mapmodalVisible)
              }, 3000)
            }
          },
        ],
        { cancelable: false }
      )
    }
    else {
      this.setMapModalVisible(!this.state.mapmodalVisible)
    }
  }

  _startWalking() {
    const marginTop = Platform.OS == 'ios' ? isIphoneX() ? 75 : 60 : 35
    return (
      <Modal
        animationType="none"
        transparent={true}
        visible={this.state.mapmodalVisible}
        onRequestClose={() => {
          this.setMapModalVisible(!this.state.mapmodalVisible)
        }}>
        <View style={{ flex: 1, width: '100%', backgroundColor: 'rgba(255,255,255,0.9)', marginTop: marginTop }}>
          <View style={{ paddingHorizontal: 30, width: '100%', marginTop: 20, }}>
            <View style={{ marginTop: 10, width: '100%' }}>
              <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#000', fontFamily: 'Lato-Bold' }}>NOTICE</Text>
              <ScrollView
                contentContainerStyle={{ paddingBottom: '40%' }} style={{ marginTop: 15, }}
                showsVerticalScrollIndicator={false}>
                <Text style={{ color: '#000', fontSize: 18, width: '90%', fontFamily: 'Lato-Regular', fontStyle: 'italic' }}>
                  Do NOT close the app or turn off your phone during the walk.{"\n"}
                  Make sure the screen will NOT go into sleep mode;{"\n"}
                  the walk will <Text style={{ color: '#ac014f', fontSize: 18, width: '90%', fontFamily: 'Lato-Regular', fontStyle: 'italic' }}>NOT</Text> be saved!
               </Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                  <Text style={{ color: '#000', fontSize: 18, fontFamily: 'Lato-Regular', fontStyle: 'italic' }}>
                    When not walking, press {" "}
                  </Text>
                  <TouchableOpacity disabled style={{ alignItems: 'center', backgroundColor: '#cfc4c0', height: 20, width: 20, borderRadius: 10, justifyContent: 'center' }}>
                    <Icon name='ios-pause' type={'Ionicons'} style={{ color: 'white', fontSize: 10 }} />
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                  <Text style={{ color: '#000', fontSize: 18, fontFamily: 'Lato-Regular', fontStyle: 'italic' }}>
                    When continuing the walk press {" "}</Text>
                  <TouchableOpacity disabled style={{ alignItems: 'center', backgroundColor: '#cfc4c0', height: 20, width: 20, borderRadius: 10, justifyContent: 'center' }}>
                    <Icon name='ios-play' type={'Ionicons'} style={{ color: 'white', fontSize: 10 }} />
                  </TouchableOpacity>
                </View>
                <Text style={{ marginTop: 10, color: '#000', fontSize: 18, width: '90%', fontFamily: 'Lato-Regular', fontStyle: 'italic' }}>
                  In case the GPS tracking is showing a wrong line, correct by pressing <Text style={{ fontSize: 22, fontStyle: 'normal' }}>↺</Text> as often as necessary</Text>
                <Text style={{ marginTop: 10, color: '#000', fontSize: 18, width: '90%', fontFamily: 'Lato-Regular', fontStyle: 'italic' }}>
                  For a complete manual, go to HELP in MENU
              </Text>
                <Text style={{ marginTop: 10, color: '#000', fontSize: 18, width: '90%', fontFamily: 'Lato-Regular', fontStyle: 'italic' }}>
                  Be safe, be mindful and respectful of others. We wish you an unforgettable experience.
              </Text>
              </ScrollView>
            </View>
          </View>
          <View style={[styles.selectPhotoView, { bottom: 20, position: 'absolute', paddingHorizontal: 30 }]}>
            <TouchableOpacity
              onPress={() => { this.setMapModalVisible(!this.state.mapmodalVisible), Actions.Map({ id: this.state.walkId, _name: this.state.name, startWalk: this.props.startWalk }) }} style={[styles.photoBtn, { marginBottom: 5, backgroundColor: '#B2CE38', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }]}>
              <Icon name='md-checkmark' type='Ionicons' style={{ fontSize: 25, color: '#fff', right: 10 }} />
              <Text style={styles.startWalkTxt}>I UNDERSTAND</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{ position: 'absolute', top: 20, right: 20 }}
            onPress={() => { this.setMapModalVisible(!this.state.mapmodalVisible), this.props.startWalk ? Actions.reset("Home") : null }}>
            <View style={styles.plusIconSideView}>
              <Icon name='close' type='MaterialCommunityIcons' style={[{ color: '#746764', fontSize: 25 }]} />
            </View>
          </TouchableOpacity>

        </View>

      </Modal>
    )
  }

  _deleteProfileData() {
    var apiData = new FormData()
    apiData.append('walk_id', this.state.walkId)
    Api.post('/walk/delete_walk', apiData)
      .then((result) => {
        if (result.result_code == 200) {
          this.setModalVisible(!this.state.modalVisible)
          Actions.Home()
        }
      })
  }

  _deleteProfile() {
    return (
      <Modal
        animationType='none'
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setModalVisible(!this.state.modalVisible)
        }}>
        <View style={{ height: Dimensions.get('window').height, width: '100%', backgroundColor: 'rgba(255,255,255,0.9)', marginTop: Platform.OS == 'ios' ? 60 : 35 }}>
          <View style={{ width: '100%', height: '100%', marginTop: 20, backgroundColor: 'transparent' }}>
            <View style={{ marginVertical: 30, width: '100%' }}>
              <View style={[styles.interactSideView, { justifyContent: 'flex-end' }]}>
                <TouchableOpacity onPress={() => this.setModalVisible(!this.state.modalVisible)}>
                  <View style={styles.plusIconSideView}>
                    <Icon name='close' type='MaterialCommunityIcons' style={[{ color: '#746764', fontSize: 25 }]} />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ paddingHorizontal: 30, marginTop: 10, width: '100%' }}>
              <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#000', fontFamily: 'Lato-Bold' }}>DELETE THIS PROFILE</Text>
              <Text style={{ marginTop: 15, color: '#000', fontSize: 18, width: '90%', fontFamily: 'Lato-Regular' }}>Are you sure you want to delete this profile?</Text>
              <Text style={{ marginTop: 15, color: '#000', fontSize: 18, width: '90%', fontFamily: 'Lato-Regular' }}>
                You will not be able to restore it, and all walks, images and data related to this profile will be permanently removed.
              </Text>
            </View>
            <View style={styles.selectPhotoView}>
              <TouchableOpacity onPress={() => { this._deleteProfileData() }} style={[styles.photoBtn, { marginBottom: 5, backgroundColor: '#A50050' }]}>
                <Text style={styles.selectPhotoTxt}>I'M SURE, DELETE THIS PROFILE</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.setModalVisible(!this.state.modalVisible)} style={[styles.photoBtn, { backgroundColor: '#C5BDB8' }]}>
                <Text style={styles.selectPhotoTxt}>NO, GO BACK</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ width: '100%', bottom: 75, position: 'absolute' }}>
            <View style={{ paddingLeft: 25 }}>
              <TouchableOpacity onPress={() => { this._deleteProfileData() }}>
                <Image source={require('../assets/images/delete_1.png')} style={styles.bottomBtn} />
              </TouchableOpacity>
            </View>
          </View>
        </View>

      </Modal>
    )
  }

  _goToTop = () => {
    this.scroll.scrollTo({ x: 0, y: 0, animated: true });
  }

  _scrollup = () => {
    this.scroll.scrollTo({ x: 0, y: 100, animated: false });
  }

  _scrollup1 = () => {
    console.log('ok')
    this.scroll.scrollTo({ x: 0, y: 180, animated: false });
  }
  _scrollup2 = () => {
    console.log('ok')
    this.scroll.scrollTo({ x: 0, y: 220, animated: false });
  }

  _datePicker(type) {
    Keyboard.dismiss()
    try {
      DatePickerAndroid.open({ date: new Date() })
        .then((date) => {
          if (date.action != 'dismissedAction') {
            type == 'dob' ? this.setState({ dob: (date.month + 1) + '/' + date.day + '/' + date.year, saveAlert: true }) : this.setState({ dop: (date.month + 1) + '/' + date.day + '/' + date.year, saveAlert: true })
          }
        })
    } catch ({ code, message }) {
      console.warn('Cannot open date picker', message);
    }
  }

  _bottomBtnView() {
    return (
      <View style={{ width: '100%', alignItems: 'center', bottom: 20, position: 'absolute' }}>
        <View style={styles.btnView}>
          <View>
            <TouchableOpacity onPress={() => this.setModalVisible(!this.state.modalVisible)}>
              <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: '#a60150', alignItems: 'center', justifyContent: 'center' }}>
                <Icon name='trash' type='FontAwesome5' style={{ color: '#fff', fontSize: 18 }} />
              </View>
            </TouchableOpacity>
          </View>
          <View>
            {this.state.loading == false ?
              <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: '#4d6a26', alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator color="#fff" size='small' />
              </View> :
              <TouchableOpacity disabled={this.state.backgroundImage == null || this.state.profile.path == undefined || this.state.about == '' || this.state.name == '' ? true : false} onPress={() => this._add()}>
                {/* <Image source={require('../assets/images/save_1.png')} style={styles.bottomBtn} /> */}
                <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: '#4d6a26', alignItems: 'center', justifyContent: 'center', opacity: this.state.backgroundImage == null || this.state.profile.path == undefined || this.state.about == '' || this.state.name == '' ? 0.3 : 1 }}>
                  <Text style={{ fontFamily: 'Lato-Bold', color: '#fff', fontSize: 15 }}>SAVE</Text>
                </View>
              </TouchableOpacity>
            }
          </View>
          <View>
            <TouchableOpacity disabled={this.state.walkId ? false : true} onPress={() => this.setMapModalVisible(!this.state.mapmodalVisible)}>
              <Image source={require('../assets/images/startwalkingenable.png')} style={[styles.bottomBtn, { opacity: this.state.walkId ? 1 : 0.3 }]} />
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity>
              <Image source={require('../assets/images/edit.png')} style={[styles.bottomBtn, { borderWidth: 3, borderColor: '#3F9DC8', borderRadius: 50 / 2 }]} />
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={this._goToTop} >
              <Image source={require('../assets/images/scrollupenable.png')} style={styles.bottomBtn} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  render() {
    if (this.state.indicator) {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator color="#746761" size='large' />
        </View>
      )
    }
    else {
      return (
        <View style={styles.container}>
          {this.state.modalVisible || this.state.mapmodalVisible ? <Header /> : <Header back={true} backToHome={this.props.walk ? false : true} data={() => this._headerBackpress()} />}
          <ImageBackground progressiveRenderingEnabled={true} source={{ uri: this.state.backgroundImage ? this.state.backgroundImage.background_url : '' }} style={{ flex: 1 }} >
            {/* <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? 'position' : null} style={{ width: '100%' }}> */}
            <ScrollView ref={(c) => { this.scroll = c }} onScroll={(e) => console.log(e.nativeEvent.contentOffset.x, e.nativeEvent.contentOffset.y)} showsVerticalScrollIndicator={false} contentContainerStyle={{ width: '100%', alignItems: 'center', paddingBottom: '20%' }}>
              <View style={styles.secondaryView}>
                <View style={styles.interactSideView}>
                  <TouchableOpacity style={{ opacity: this.state.walkId ? 1 : 0.3 }} disabled={this.state.walkId ? false : true} onPress={() => this.setModalVisible(!this.state.modalVisible)}>
                    <View style={[styles.plusIconSideView, { backgroundColor: '#a60150' }]}>
                      <Icon name='trash' type='FontAwesome5' style={{ color: '#fff', fontSize: 15 }} />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => Actions.AddBackground({ addBackground: this._addBackground })}>
                    <View style={styles.plusIconSideView}>
                      <Icon name={this.state.backgroundImage ? 'edit' : 'plus'} type={this.state.backgroundImage ? 'MaterialIcons' : 'FontAwesome'} style={styles.icon} />
                    </View>
                  </TouchableOpacity>
                </View>
                {this.state.profile ?
                  <View style={styles.profilePictureView}>
                    <Image source={{ uri: this.state.profile.path }} style={{ height: 150, width: 150, borderRadius: 75 }} />
                    <TouchableOpacity onPress={() => this._profile()} style={styles.interactView}>
                      <View style={styles.plusIconView}>
                        <Icon name='edit' type='MaterialIcons' style={styles.icon} />
                      </View>
                    </TouchableOpacity>
                  </View>
                  : <View style={styles.profilePictureView}>
                    <TouchableOpacity onPress={() => this._profile()} style={styles.interactView}>
                      <View style={styles.plusIconView}>
                        <Icon name='plus' type='FontAwesome' style={styles.icon} />
                      </View>
                    </TouchableOpacity>
                  </View>}
              </View>

              <View style={[styles.txtInputView]}>

                <TextInput
                  style={[styles.txtInput, { marginBottom: 10 }]}
                  placeholder='name of the deceased*'
                  onFocus={() => { this.setState({ dateOfPass: false, dateOfBirth: false, focus: true }), Platform.OS == 'ios' ? this._scrollup() : null }}
                  onChangeText={(text) => this.setState({ name: text, saveAlert: true })}
                  value={this.state.name}
                  onBlur={() => this.setState({ focus: false })}
                />

                <TextInput
                  style={[styles.txtInput, { marginBottom: 10 }]}
                  placeholder='date of birth*'
                  onFocus={() => { Keyboard.dismiss(), Platform.OS == 'ios' ? this.setState({ dateOfBirth: true, dateOfPass: false }) : this._datePicker('dob'), Platform.OS == 'ios' ? this._scrollup() : null }}
                  onChangeText={(text) => this.setState({ dob: text })}
                  value={this.state.dob ? moment(this.state.dob).format('MMMM/DD/YYYY') : ''}
                />
                {this.state.dateOfBirth ?
                  <DatePickerIOS
                    style={{ backgroundColor: '#fff', marginBottom: 7, color: 'black' }}
                    mode='date'
                    initialDate={this.state.dob ? new Date(this.state.dob) : new Date()}
                    onDateChange={(date) => {
                      this.setState({ dob: moment(date).format('MM/DD/YYYY'), saveAlert: true })
                    }}
                  />
                  : null}

                <TextInput
                  style={[styles.txtInput, { marginBottom: 10 }]}
                  placeholder='date of passing*'
                  onFocus={() => { Keyboard.dismiss(), Platform.OS == 'ios' ? this.setState({ dateOfPass: true, dateOfBirth: false }) : this._datePicker('dop'), Platform.OS == 'ios' ? this._scrollup1() : null }}
                  onChangeText={(text) => this.setState({ dop: text })}
                  value={this.state.dop ? moment(this.state.dop).format('MMMM/DD/YYYY') : ''}
                />
                {this.state.dateOfPass ?
                  <DatePickerIOS
                    style={{ backgroundColor: '#fff', marginBottom: 7, color: 'black' }}
                    mode='date'
                    initialDate={this.state.dop ? new Date(this.state.dop) : new Date()}
                    onDateChange={(date) => {
                      this.setState({ dop: moment(date).format('MM/DD/YYYY').toString(), saveAlert: true })
                    }}
                  /> : null}

                <TextInput
                  style={[styles.aboutTxtInput]}
                  onFocus={() => { this.setState({ dateOfPass: false, dateOfBirth: false, focus: true }), Platform.OS == 'ios' ? this._scrollup2() : null }}
                  placeholder='about this person'
                  textAlignVertical='top'
                  multiline={true}
                  onChangeText={(text) => this.setState({ about: text, saveAlert: true })}
                  value={this.state.about}
                  onBlur={() => this.setState({ focus: false })}
                />

              </View>
            </ScrollView>
            <View style={{ justifyContent: 'flex-end', width: '100%', flexDirection: 'row' }}>
              {this.state.loading == false ?
                <View style={{ width: '50%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#4d6a26', flexDirection: 'row', height: 40 }}>
                  <ActivityIndicator color="#fff" size='small' />
                </View> :
                <TouchableOpacity style={{ width: '50%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#4d6a26', flexDirection: 'row', height: 40, opacity: this.state.name && this.state.dob && this.state.dop ? 1 : 0.7 }} disabled={this.state.name == '' || this.state.dob == '' || this.state.dop == '' ? true : false} onPress={() => this._add()}>
                  <Text style={[styles.startWalkTxt]}>SAVE</Text>
                </TouchableOpacity>
              }
              <TouchableOpacity style={{ width: '50%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#b0ce4b', opacity: this.state.walkId ? 1 : 0.5, flexDirection: 'row' }} disabled={this.state.walkId ? false : true} onPress={() => this._savedAlert()}>
                <Image source={require('../assets/images/startwalkingenable.png')} style={{ height: 40, width: 40, opacity: this.state.walkId ? 1 : 0.2 }} />
                <Text style={[styles.startWalkTxt, { opacity: this.state.walkId ? 1 : 0.2 }]}>START WALK</Text>
              </TouchableOpacity>
            </View>
            {this._deleteProfile()}
            {this._startWalking()}
            <MessageBar ref={(ref) => this.messageBar = ref} message={this.state.message} color={this.state.color} />
          </ImageBackground>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(236,234,235)',
  },
  secondaryView: {
    marginVertical: 30,
    width: '100%'
  },
  profilePictureView: {
    width: 150,
    height: 150,
    borderRadius: 150 / 2,
    backgroundColor: '#FAFAFA',
    alignSelf: 'center'
  },
  interactSideView: {
    paddingHorizontal: 10,
    marginTop: -10,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  interactView: {
    position: 'absolute',
    bottom: 0,
    width: '90%',
    alignItems: 'flex-end'
  },
  plusIconSideView: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 35,
    height: 35,
    borderRadius: 35 / 2,
    backgroundColor: '#fff',
    borderWidth: 0.5,
    borderColor: '#EEEEEE',
    elevation: 20
  },
  plusIconView: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: '#fff',
    borderWidth: 0.5,
    borderColor: '#EEEEEE',
    elevation: 20
  },
  icon: {
    color: '#746764',
    fontSize: 15
  },
  txtInputView: {
    // marginTop: 10,
    width: '80%'
  },
  txtInput: {
    paddingLeft: 15,
    height: 50,
    backgroundColor: '#F4F4F4',
    borderWidth: 0.5,
    borderColor: '#F4F4F4',
    fontSize: 15,
    fontFamily: 'Lato-Regular'
  },
  aboutTxtInput: {
    paddingLeft: 15,
    height: 230,
    backgroundColor: '#F4F4F4',
    borderWidth: 0.5,
    borderColor: '#F4F4F4',
    fontSize: 15,
    fontFamily: 'Lato-Regular'
  },
  btnView: {
    justifyContent: 'space-between',
    width: '85%',
    alignItems: 'center',
    flexDirection: 'row'
  },
  bottomBtn: {
    height: 50,
    width: 50
  },
  selectPhotoView: {
    width: '100%',
    alignSelf: 'center',
    marginTop: 40
  },
  photoBtn: {
    height: 40,
    width: '100%',
    justifyContent: 'center'
  },
  selectPhotoTxt: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'Lato-Bold'
  },
  startWalkTxt: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 15,
    fontWeight: 'bold',
    fontFamily: 'Lato-Bold'
  },
});
