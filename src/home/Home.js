import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, TouchableOpacity, Image, AsyncStorage, Animated, Dimensions } from 'react-native';
import Header from '../components/Header'
import Api from '../components/api'
import { Icon } from 'native-base';
import WalkView from '../home/WalkView'
import { Actions } from 'react-native-router-flux';
var width = Dimensions.get('window').width
export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      walk: [1, 2, 3, 4, 5, 6, 7, 8],
      profile: this.props.profile,
      height: new Animated.Value(0)
    }
  }
  _getWalk() {
    AsyncStorage.getItem('userData')
      .then((userData) => {
        var user = JSON.parse(userData)
        if (user) {
          this.setState({ profile: user.photo_url })
          var apiData = new FormData()
          apiData.append('user_id', user.id)
          Api.post('/walk/get_walks', apiData)
            .then((result) => {
              this.setState({ walk: result.walk_list })
            })
        }
      })
  }
  componentDidMount() {
    this._getWalk()
  }

  _renderItem(item, index) {
    return (
      <View style={{ width: '50%', alignItems: 'center', paddingVertical: 10 }}>
        <TouchableOpacity onPress={() => this.props.nav(<WalkView backNav={this.props.backNav} data={this.state.walk[index]} index={index} openDrawer={this.props.openDrawer} />, this.state.walk[index])} style={{ backgroundColor: item == 1 ? 'rgb(255,255,255)' : 'rgb(241,241,241)', width: width / 2.5, height: width / 2.5, borderRadius: width / 5, justifyContent: 'center', alignItems: 'center' }}>
          {item.profile_url ?
            <View>
              <Image source={{ uri: item.profile_url }} style={{ width: width / 2.5, height: width / 2.5, borderRadius: width / 5 }} />

            </View>
            :
            null}
          <TouchableOpacity style={styles.interactViewWalk} onPress={() => Actions.AddPerson({ personData: item, walk: () => this._getWalk(), startWalk: true })}>
            <View style={styles.plusIconView}>
              <Image resizeMode='cover' source={require('../assets/images/walk.png')} style={{ height: 24, width: 15, tintColor: '#ac014f' }} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.interactView} onPress={() => Actions.AddPerson({ personData: item, walk: () => this._getWalk() })}>
            <View style={styles.plusIconView}>
              <Icon name='edit' type='MaterialIcons' style={styles.icon} />
            </View>
          </TouchableOpacity>
        </TouchableOpacity >

      </View>
    )
  }
  render() {
    const marginBottom = this.state.height.interpolate({
      inputRange: [100, 150, 200],
      outputRange: [10, -35, -65],
      extrapolate: 'clamp'
    })
    return (
      <View style={styles.container}>
        <Header profile={this.state.profile} menu={true} openDrawer={this.props.openDrawer} />
        <FlatList
          data={this.state.walk}
          contentContainerStyle={{ width: '100%', paddingHorizontal: 10 }}
          extraData={this.state}
          numColumns={2}
          onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.state.height } } }])}
          renderItem={({ item, index }) => this._renderItem(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
        <Animated.View style={{ paddingHorizontal: 20, bottom: marginBottom, position: 'absolute', alignSelf: 'flex-end', elevation: 1.5 }}>
          <TouchableOpacity onPress={() => Actions.AddPerson({ walk: () => this._getWalk() })} style={{ height: 60, width: 60, borderRadius: 30, backgroundColor: '#ffffff', justifyContent: 'center', alignItems: 'center' }}>
            <Icon name="plus" type="Entypo" style={{ fontSize: 30, color: '#786a64' }} />
          </TouchableOpacity>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(236,234,235)',
  },
  interactView: {
    position: 'absolute',
    bottom: 5,
    right: 10,
  },
  interactViewWalk: {
    position: 'absolute',
    bottom: 5,
    left: 10,
  },
  icon: {
    color: '#746764',
    fontSize: 15
  },
  plusIconView: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 35,
    height: 35,
    borderRadius: 35 / 2,
    backgroundColor: '#fff',
    borderWidth: 0.5,
    borderColor: '#EEEEEE',
    elevation: 8
  },
});
