import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Dimensions,
    ImageBackground,
    ActivityIndicator,
    ScrollView,
    StatusBar
} from 'react-native';
import { Icon, Item, Header } from 'native-base';
import { Actions } from 'react-native-router-flux';
import MapView, { PROVIDER_GOOGLE, Polyline, Marker } from 'react-native-maps';
import Swiper from 'react-native-swiper';
import Api from '../components/api'
import Geolocation from 'react-native-geolocation-service';
var _ = require('lodash');
import moment from 'moment'

var bottom = Platform.select({
    ios: Math.round(Dimensions.get('window').height / 1.25),
    android: Math.round(Dimensions.get('window').height / 1.28)
})
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
var lastlatlog = {}
export default class WalkView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            backgrounds: [require('../assets/images/background1.jpeg'),
            require('../assets/images/background2.jpg'),
            require('../assets/images/background3.jpg'),
            require('../assets/images/background4.jpg'),
            require('../assets/images/background5.jpg')
            ],
            selected: 0,
            profileImg: [
                require('../assets/images/sample1.jpeg'),
                require('../assets/images/sample2.jpeg'),
                require('../assets/images/sample3.jpg'),
                require('../assets/images/sample4.jpg'),
                require('../assets/images/sample5.jpg')
            ],
            data: [],
            indicator: true,
            location_list: [],
            latlong: {},
            currentlatlong: {},
            displayWalk: [],
            latitude: 37.78825,
            longitude: -122.4324,
        }
    }

    componentWillMount() {
        Geolocation.getCurrentPosition((position) => {
            var coords = { latitude: position.coords.latitude, longitude: position.coords.longitude }
            var currentlatlong = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            }
            var apiData = new FormData()
            apiData.append('walk_id', this.props.data.id)
            Api.post('/walk/get_location', apiData)
                .then((result) => {
                    var walk_number = function (d) {
                        return d.walk_number;
                    }
                    var latlong = function (d) {
                        return d.map(item => {
                            return {
                                latitude: Number(item.latitude),
                                longitude: Number(item.longitude)
                            };
                        })
                    }
                    var groupToSummary = function (group) {
                        return latlong(group)
                    }
                    var location_list = []
                    if (result.location_list.length > 0) {
                        location_list = _(result.location_list)
                            .groupBy(walk_number)
                            .map(groupToSummary)
                            .value();
                        var displayWalk = location_list[0]
                    }
                    lastlatlog = location_list.length > 0 ?
                        location_list[0][0] : currentlatlong
                    if (result.result_code == 200) {
                        this.setState({ location_list: location_list[0], latlong: lastlatlog, displayWalk: displayWalk, indicator: false }, () => console.log('AFTER SETSTTE', this.state.latlong, this.state.displayWalk))
                    }
                })
        },
            (error) => console.log('Location Error', error),
            {
                enableHighAccuracy: true,
                timeout: 20000,
                maximumAge: 1000
            }
        )

        this.setState({ data: this.props.data })
    }

    render() {
        if (this.state.indicator) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator color="#746761" size='large' />
                </View>
            )
        }
        else {
            return (
                <View style={styles.container}>
                    <Header style={{ width: '100%', backgroundColor: 'white' }} transparent >
                        <TouchableOpacity onPress={() => this.props.backNav()} style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='ios-arrow-back' type="Ionicons" style={{ color: '#7d716c', fontWeight: '100' }} />
                        </TouchableOpacity>
                        <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../assets/images/headerImage.png')} style={{ height: 14, width: 150 }} />
                        </View>
                        <TouchableOpacity onPress={this.props.openDrawer} style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='ios-menu' type='Ionicons' />
                        </TouchableOpacity>
                    </Header>
                    <StatusBar backgroundColor='white' barStyle='dark-content' />
                    <Swiper
                        onIndexChanged={(index) => this.setState({ selected: index })}
                        dot={<View style={{ backgroundColor: this.state.selected == 0 ? 'rgba(255,255,255,0.6)' : '#c8c6c7', width: 13, height: 13, borderRadius: 7, marginLeft: 7, marginRight: 7 }} />}
                        activeDot={<View style={{ backgroundColor: this.state.selected == 0 ? '#fff' : '#736b68', width: 13, height: 13, borderRadius: 7, marginLeft: 7, marginRight: 7 }} />}
                        paginationStyle={{ bottom: 20 }}
                        loop={false}
                    >
                        <ImageBackground source={{ uri: this.state.data.background_url }} style={styles.backImageView} >
                            <View style={[styles.secondaryView]}>
                                <View style={{ width: width / 1.2, marginTop: 25 }}>
                                    <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 19, textAlign: 'center' }}>{this.state.data.deceased_name}</Text>
                                </View>
                                <TouchableOpacity onPress={() => Actions.ProfileImage({ fullImage: this.state.data.profile_url, title: this.state.data.deceased_name })} style={[styles.profileImageView, { marginTop: 35 }]}>
                                    <Image source={{ uri: this.state.data.profile_url }} style={styles.image} />
                                </TouchableOpacity>
                                <View style={styles.roundMapView}>
                                    <TouchableOpacity onPress={() => Actions.Map({ _name: this.state.data.deceased_name, id: this.state.data.id, location: this.state.location_list })}>
                                        <MapView
                                            provider={null}
                                            style={{
                                                height: width / 2,
                                                width: width / 2,
                                            }}
                                            region={{
                                                latitude: this.state.latlong.latitude,
                                                longitude: this.state.latlong.longitude,
                                                latitudeDelta: Platform.OS == 'android' ? 0.000534175951948157 : 0.000534175951948157,
                                                longitudeDelta: Platform.OS == 'android' ? 0.00042915344236860165 : 0.00042915344236860165,
                                            }}
                                        >
                                            {this.state.displayWalk ?
                                                <Polyline
                                                    coordinates={this.state.displayWalk}
                                                    strokeColor="#78b5f7"
                                                    strokeWidth={5}
                                                /> : null
                                            }
                                        </MapView>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: width / 1.1, paddingTop: 10, position: 'absolute', bottom: 20, }}>
                                    <Text style={[styles.dateTxt, { textAlign: 'left', width: 120 }]}>
                                        {moment(this.state.data.birth_date).format('DD MMM YYYY')}
                                    </Text>
                                    <Text style={[styles.dateTxt, { textAlign: 'right', width: 120 }]}>
                                        {moment(this.state.data.passing_date).format('DD MMM YYYY')}
                                    </Text>
                                </View>
                            </View>
                        </ImageBackground>
                        <View style={{ flex: 1, backgroundColor: '#f0eeef' }}>
                            <ScrollView ref={(c) => { this.scroll = c }} onScroll={(e) => console.log(e.nativeEvent.contentOffset.x, e.nativeEvent.contentOffset.y)} showsVerticalScrollIndicator={true} contentContainerStyle={{ width: '100%', alignItems: 'center', paddingBottom: '20%' }}>
                                <View style={{ width: width / 1.2, marginTop: 25 }}>
                                    <Text style={{ color: '#cb4680', fontWeight: 'bold', fontSize: 19, textAlign: 'center' }}>{this.state.data.deceased_name}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', width: width / 1.2, paddingTop: 20, justifyContent: 'space-between' }}>
                                    <Text style={[styles.dateTxt, { color: '#000', textAlign: 'left', width: 100 }]}>
                                        {moment(this.state.data.birth_date).format('DD MMM YYYY')}
                                    </Text>
                                    <Text style={{ fontSize: 14, color: '#000', fontWeight: '500', textAlign: 'center' }}>.</Text>
                                    <Text style={[styles.dateTxt, { color: '#000', textAlign: 'right', width: 100 }]}>
                                        {moment(this.state.data.passing_date).format('DD MMM YYYY')}
                                    </Text>
                                </View>
                                <View style={{ marginTop: 30, width: width / 1.2 }}>
                                    <Text style={{ fontSize: 16, fontWeight: '400', lineHeight: 20, fontFamily: 'Lato-Regular' }}>{this.state.data.about_person}</Text>
                                </View>
                            </ScrollView>
                        </View>
                    </Swiper>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
        width: '100%',
        height: height / 2
    },
    selectPhotoView: {
        width: '85%',
        alignSelf: 'center',
    },
    photoBtn: {
        height: 40,
        backgroundColor: '#726761',
        width: '100%',
        justifyContent: 'center'
    },
    selectPhotoTxt: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'Lato-Bold'
    },
    dot: {
        bottom: bottom,
        height: 12,
        width: 12,
        borderRadius: 6,
    },
    backImageView: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    secondaryView: {
        width: '100%',
        alignItems: 'center',
        flex: 1,
    },
    profileImageView: {
        backgroundColor: '#f4f4f4',
        height: height / 2,
        width: width / 1.2,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 30
    },
    roundMapView: {
        backgroundColor: '#ffffff',
        height: width / 2,
        width: width / 2,
        borderRadius: width / 4,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -90,
        elevation: 1,
        overflow: 'hidden'
    },
    dateTxt: {
        textAlign: 'center',
        width: 75,
        fontSize: 14,
        color: '#fff',
        fontWeight: '400',
        fontFamily: 'Lato-Regular',
        flexWrap: 'wrap',
    }
});