import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import Swiper from 'react-native-swiper';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-crop-picker';
import Api from '../components/api'
import Header from '../components/Header'
export default class AddBackground extends Component {
    constructor(props) {
        super(props)
        this.state = {
            backgrounds: [],
            selected: 0,
            indicator: true
        }
    }
    _add() {
        this.props.addBackground(this.state.backgrounds[this.state.selected])
        Actions.pop()
    }
    componentDidMount() {
        Api.post('/walk/get_background_image', null)
            .then((result) => {
                this.setState({ indicator: false, backgrounds: result.background_images })
            })
    }
    _captureBackground() {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            this.setState(state => (this.state.backgrounds.unshift({ background_url: image.path }), state))
        });
    }
    render() {
        var bottom = Platform.select({
            ios: Math.round(Dimensions.get('window').height / 1.25),
            android: Math.round(Dimensions.get('window').height / 1.28)
        })
        if (this.state.indicator) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator color="#746761" size='large' />
                </View>
            )
        }
        else {
            console.log('backgournd', this.state.backgrounds)
            return (
                <View style={styles.container}>
                    <Header back={true} />
                    <Swiper loop={false} removeClippedSubviews={true} onIndexChanged={(index) => this.setState({ selected: index })} showsButtons={true} buttonWrapperStyle={{ paddingHorizontal: 40 }} prevButton={<Icon name='ios-arrow-back' type='Ionicons' style={{ color: 'white', fontSize: 35 }} />} nextButton={<Icon name='ios-arrow-forward' type='Ionicons' style={{ color: 'white', fontSize: 35 }} />} dotStyle={{ bottom: bottom, height: 12, width: 12, borderRadius: 6, backgroundColor: 'rgba(255,255,255,0.2)' }} activeDotStyle={{ bottom: bottom, backgroundColor: '#fff', height: 12, width: 12, borderRadius: 6 }}>
                        {this.state.backgrounds.map((item, index) =>
                            <ImageBackground key={index} source={{ uri: item.background_url }} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }} >
                                <View style={{ height: '80%', width: '85%', backgroundColor: 'rgba(255,255,255,0.5)', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                                    <Text style={{ fontSize: 24, color: 'white', textAlign: 'center', width: '80%', fontFamily: 'Lato-Regular' }}>Swipe left & right to choose your background</Text>
                                </View>
                                <View style={{ width: '90%', marginTop: -20, flexDirection: 'row' }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', width: '50%' }}>
                                        <TouchableOpacity onPress={() => this._add()} style={{ alignItems: 'center', justifyContent: 'center' }}>
                                            <View style={{ height: 40, width: 40, borderRadius: 20, backgroundColor: '#4d6a26', alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ fontFamily: 'Lato-Bold', color: '#fff', fontSize: 11 }}>SAVE</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: '50%' }}>
                                        <TouchableOpacity onPress={() => this._captureBackground()} style={{ backgroundColor: 'white', height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center' }}>
                                            <Icon name='camera' type='SimpleLineIcons' style={{ fontSize: 22 }} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </ImageBackground>
                        )}
                    </Swiper>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

});
