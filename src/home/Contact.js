import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    StatusBar
} from 'react-native';

import Header from '../components/Header'

export default class Contact extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'rgb(236,234,235)' }}>  
                <Header back={true} />
                <StatusBar backgroundColor='white' barStyle='dark-content' />
                <View style={{ width: '100%', alignItems: 'center', flex: 1 }}>

                    <View style={{ marginVertical: 30, width: '75%', alignItems: 'flex-start' }}>
                        <Text style={{ fontSize: 18, fontFamily: 'Lato-Bold' }}>Contact Us</Text>
                        <Text style={{ fontSize: 15, fontFamily: 'Lato-Regular', paddingVertical: 15 }}>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at walkwith@tolad-one.com.</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
})