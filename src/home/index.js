import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import {Drawer} from 'native-base'
import Home from './Home'
import Sidebar from '../components/SideBar'


export default class Index extends Component {
    constructor(props){
        super(props)
        this.state={
            profile:this.props.profile,
            currentScene: '',
            personalDetails:'',
            userData:''
        }
        this._navigate = this._navigate.bind(this)
        this._navigateBack = this._navigateBack.bind(this)
    }
    closeDrawer() {
        this.drawer._root.close()
    }
    openDrawer() {
        this.drawer._root.open()
    }
    _navigate(details,data){
        this.setState({personalDetails:details,userData:data})
    }
    _navigateBack(){
        this.setState({personalDetails:''})
    }
    render() {
        return (
            <View style={styles.container}>
               <Drawer
                    ref={(ref) => this.drawer = ref}
                    content={<Sidebar details={this.state.personalDetails ? true : false} data={this.state.userData} closeDrawer={()=>this.closeDrawer()} />}
                    tapToClose={true}
                    panThreshold={0.5}
                    panOpenMask={0.5}
                    side='right'
                    openDrawerOffset={0.2} // 20% gap on the right side of drawer
                    tweenHandler={(ratio) => ({
                        mainOverlay: { opacity:ratio/1.5,backgroundColor:'white' }
                      })}
                    >
                    {this.state.personalDetails ? this.state.personalDetails : <Home backNav={this._navigateBack} nav={this._navigate} profile={this.state.profile} data={this.props.Data} openDrawer={() => this.openDrawer()} /> }
                </Drawer>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
});
