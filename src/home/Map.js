import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  Image,
  PermissionsAndroid,
  Dimensions,
  AsyncStorage,
  ActivityIndicator,
  NetInfo
} from 'react-native';
import MapView, { PROVIDER_GOOGLE, Polyline, Marker } from 'react-native-maps';
import Header from '../components/Header'
import { Icon, Body } from 'native-base';
import Api from '../components/api'
import MessageBar from '../components/messageBar'
import RNLocation from 'react-native-location';
import { Actions } from 'react-native-router-flux';
const GOOGLE_MAPS_APIKEY = 'AIzaSyBgTODnIoLps14gJxxWXLQmw4OhMq_KkpA';
var locationInterval
var location = []
var time = ''
var distance = ''
var path = []
var pathData = []
var watchID = 0
export default class Map extends Component {
  constructor(props) {
    super(props)
    this.state = {
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0011332582093857013,
      longitudeDelta: 0.000730566680431366,
      polyline: [],
      show: true,
      userName: '',
      mapType: 'standard',
      loading: true,
      walk_number: 1,
      walkingPath: [],
      speed: 0,
      counter: 0,
      currLatLong: null,
      internetConnection: true

    }
    this.locationSubscription = ''
  }

  componentWillMount() {
    RNLocation.configure({
      distanceFilter: 1.0,
      desiredAccuracy: {
        ios: "best",
        android: "balancedPowerAccuracy"
      },
      // Android only
      androidProvider: "auto",
      interval: 1000, // Milliseconds
      fastestInterval: 10000, // Milliseconds
      maxWaitTime: 2000, // Milliseconds
      // iOS Only
      activityType: "other",
      allowsBackgroundLocationUpdates: false,
      headingFilter: 1, // Degrees
      headingOrientation: "portrait",
      pausesLocationUpdatesAutomatically: false,
      showsBackgroundLocationIndicator: false,
    })
    this.setState({ polyline: [] })
    this.setState({ userName: this.props._name })
    this.setState({ walkingPath: this.props.location })


    AsyncStorage.getItem('time').then((value) => {
      time = JSON.parse(value)
    })
    AsyncStorage.getItem('distance').then((value) => {
      distance = JSON.parse(value)
    })
    if (this.props.location) {
      this.setState({
        latitude: this.props.location[0].latitude,
        longitude: this.props.location[0].longitude,
        loading: false
      })
    }
    else {
      var apiData = new FormData()
      if (this.props.id) {
        apiData.append('walk_id', this.props.id)
        console.log('data walk view-->', this.props.id)
        Api.post('/walk/get_location', apiData)
          .then((result) => {
            if (result.result_code == 200) {
              if (result.location_list.length > 0) {
                this.setState({ walk_number: Number(result.location_list[result.location_list.length - 1].walk_number) + 1 })
              }
            }
          })
      }
      RNLocation.requestPermission({
        ios: "whenInUse",
        android: {
          detail: "coarse"
        }
      }).then(granted => {
        if (granted) {
          RNLocation.getLatestLocation({ timeout: 10000 }).then(locations => {
            var coords = { latitude: locations.latitude, longitude: locations.longitude }
            this.setState(state => (
              this.state.loading = false,
              this.state.latitude = locations.latitude,
              this.state.longitude = locations.longitude,
              this.state.polyline = [...this.state.polyline, coords],
              state))
          })
        }
      })
    }
  }
  _addLocation() {
    var apiData = {
      walk_number: this.state.walk_number,
      walk_id: this.props.id,
      locations: this.state.polyline
    }
    fetch("http://18.196.178.198/api/walk/add_location", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(apiData)
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.result_code == 200) {
          clearInterval(locationInterval)
          Actions.reset("Home")
        }
      })
      .catch((e) => console.log("ERORR______", e))
  }
  _getLocation() {
    RNLocation.requestPermission({
      ios: "whenInUse",
      android: {
        detail: "coarse"
      }
    }).then(granted => {
      if (granted) {
        this.locationSubscription = RNLocation.subscribeToLocationUpdates(locations => {
          var locations = locations[0]
          var coords = { latitude: locations.latitude, longitude: locations.longitude }
          if (locations.speed >= 0.1 && this.state.show) {
            this.setState(state => (
              this.state.latitude = locations.latitude,
              this.state.longitude = locations.longitude,
              this.state.polyline = [...this.state.polyline, coords],
              state))
          }
        })

      }
    })
  }
  componentDidMount() {
    if (!this.props.location) {
      this._getLocation()
    }
  }
  componentWillUnmount() {
    if (!this.props.location) {
      this.locationSubscription()
    }
  }
  _onRegionChange(region) {
    if (!this.state.walkingPath) {
      this.setState(state => (
        this.state.latitudeDelta = region.latitudeDelta,
        this.state.longitudeDelta = region.longitudeDelta, state))
    }
  }
  _playpauseBtn() {
    this.setState({ show: !this.state.show }, () => {
      if (this.state.show) {
        this._getLocation()
      }
      else {
        this.locationSubscription()
      }
    })

  }

  _changeMapView() {
    this.setState({ mapType: this.state.mapType === 'satellite' ? 'standard' : 'satellite' })
  }

  _eraser() {
    path = this.state.polyline
    _new = path.filter((item, index) => index != path.length - 1)
    console.log("path :  ", _new, path)
    this.setState({ polyline: _new, latitude: _new[_new.length - 1].latitude, longitude: _new[_new.length - 1].longitude })
  }

  render() {
    if (this.state.loading)
      return (
        <View style={styles.loadingContainer}>
          <ActivityIndicator size='large' color='ac014f' />
        </View>
      )
    else
      return (
        <View style={styles.container}>
          <Header back={true} backToHome={this.props.startWalk ? true : false} />
          <View style={{ flex: 1 }}>

            <MapView
              provider={PROVIDER_GOOGLE}
              style={{ height: Dimensions.get('window').height, width: Dimensions.get('window').width, flex: 1 }}
              onRegionChangeComplete={(region) => this._onRegionChange(region)}
              mapType={this.state.mapType}
              region={{
                latitude: this.state.latitude,
                longitude: this.state.longitude,
                latitudeDelta: this.state.latitudeDelta,
                longitudeDelta: this.state.longitudeDelta
              }}
            >

              {this.props.location ? null : <Marker coordinate={this.state.polyline.length > 0 ? this.state.polyline[0] : { latitude: this.state.latitude, longitude: this.state.longitude }} />}
              {this.props.location ? null : this.state.polyline.length > 0 ? <Marker coordinate={{ latitude: this.state.latitude, longitude: this.state.longitude }} >
                <Image source={require('../assets/images/currentLocation.png')} style={{ height: 24, width: 15, tintColor: '#ac014f' }} />
              </Marker> : null}
              {this.state.polyline.length > 0 ? <Polyline
                coordinates={this.state.polyline}
                strokeColor="#78b5f7"
                strokeWidth={5}
              /> : null}
              {this.props.location && this.props.location.length > 0 ?
                <Polyline
                  coordinates={this.props.location}
                  //strokeColor={'#'+(Math.random()*0xFFFFFF<<0).toString(16)}
                  strokeColor="#78b5f7"
                  strokeWidth={5}
                />
                // )
                : null}
            </MapView>
            <View style={{ position: 'absolute', top: '5%', alignSelf: 'flex-end', right: 15 }}>
              <TouchableOpacity onPress={() => this._changeMapView()} style={{ alignItems: 'center', backgroundColor: '#cfc4c0', height: 40, width: 40, borderRadius: 20, justifyContent: 'center' }}>
                <Icon name='layers' type={'Entypo'} style={{ color: 'white', fontSize: 20 }} />
              </TouchableOpacity>
              {this.state.walkingPath ? null :
                <TouchableOpacity disabled={this.state.polyline.length == 1 ? true : false} onPress={() => this._eraser()} style={{ alignItems: 'center', backgroundColor: '#cfc4c0', height: 40, width: 40, borderRadius: 20, justifyContent: 'center', marginTop: 10 }}>
                  <Image source={require('../assets/images/undo.png')} style={{ tintColor: 'white', width: 35, height: 35 }} />
                </TouchableOpacity>
              }
            </View>
          </View>
          {this.state.walkingPath ? null :
            <SafeAreaView style={{ bottom: 5, position: 'absolute', width: '100%', flexDirection: "row", justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', width: '90%', justifyContent: 'space-around' }}>
                <TouchableOpacity
                  disabled={this.state.polyline.length >= 1 ? false : true}
                  onPress={() => this._addLocation()} style={{ width: '75%', opacity: this.state.polyline.length >= 1 ? 1 : 0.5, backgroundColor: '#ac014f', alignItems: 'center', padding: 14, borderRadius: 30, flexDirection: 'row', justifyContent: 'space-around', paddingHorizontal: 10 }}>
                  <Image resizeMode='cover' source={require('../assets/images/walk.png')} style={{ height: 24, width: 15, tintColor: 'white' }} />
                  <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 17, fontFamily: 'Lato-Bold' }}>SAVE AND END WALK</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this._playpauseBtn()} style={{ alignItems: 'center', backgroundColor: '#cfc4c0', height: 50, width: 50, borderRadius: 25, justifyContent: 'center' }}>
                  {this.state.show ? <Icon name='ios-pause' type={'Ionicons'} style={{ color: 'white', fontSize: 22 }} /> : <Icon name='ios-play' type={'Ionicons'} style={{ color: 'white', fontSize: 22 }} />}
                </TouchableOpacity>
              </View>

            </SafeAreaView>

          }
        </View>
      );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },

});
