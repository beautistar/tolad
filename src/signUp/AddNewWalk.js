import React, {Component} from 'react';
import {Platform, 
    StyleSheet, 
    Text, 
    View,
    ImageBackground,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import {Icon} from 'native-base'
import {Actions} from 'react-native-router-flux'
import Header from '../components/Header'
var bottom = Platform.select({
  ios: Math.round(Dimensions.get('window').height / 1.25),
  android: Math.round(Dimensions.get('window').height / 1.28)
})
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
export default class AddNewWalk extends Component {
  render() {
    return (
       <ImageBackground source={require('../assets/images/Background_img.png')} style={{flex:1,width:'100%'}} >
            <Header title='ADD A NEW WALK' rightButton='Help' />    
            <View style={styles.dot}></View>
            <View style={{width:'100%',alignItems:'center',paddingTop:30}}>
                <View style={{backgroundColor:'#f4f4f4',height:height/2,width:width/1.1,justifyContent:'center',alignItems:'center',paddingHorizontal:30}}>
                    <Text style={{fontSize:20,textAlign:'center',color:'#7b6d68',bottom:25,fontFamily:'Lato-Regular'}}>to start a new walk {'\n'} first create a walking {'\n'} profile for the deceased</Text>
                </View>
                <TouchableOpacity onPress={()=>Actions.AddPerson()} style={{backgroundColor:'#ffffff',height:width/2.5,width:width/2.5,borderRadius:width/5,justifyContent:'center',alignItems:'center',marginTop:-80,elevation:1}}>
                    <Icon name="plus" type="Entypo" style={{fontSize:40,color:'#786a64'}} />
                </TouchableOpacity>
            </View>
        </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  dot: {
    height: 12,
    width: 12,
    borderRadius: 6,
    backgroundColor:'#fff',
    alignSelf:'center',
    top:15
},
});
