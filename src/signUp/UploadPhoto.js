import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    SafeAreaView,
    ActivityIndicator
} from 'react-native';
import { Icon } from 'native-base'
import ImagePicker from 'react-native-image-crop-picker';
import { Actions } from 'react-native-router-flux';
import Api from '../components/api'
export default class UploadPhoto extends Component {
    constructor(props){
        super(props)
        this.state={
            image:'',
            indicator:false
        }
    }
    _takePhoto(){
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
          }).then(image => {
            console.log(image);
            this.setState({indicator:true})
            this._uploadProfile(image)
          });
    }
    _selectExisting(){
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
          }).then(image => {
            console.log(image);
            this.setState({indicator:true})
            this._uploadProfile(image)
          });
    }
    _uploadProfile(image){
        var apiData = new FormData()
        apiData.append("user_id",this.props.userId)
        apiData.append("photo",{uri:image.path,name:Date.now() +'.jpg',type:image.mime})
        Api.post('/user/upload_photo',apiData)
        .then((result)=>{
            this.setState({image:result.photo_url,indicator:false})
        })
        .catch((e)=>console.log('Error',e))
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.headerView}>
                    <Text style={styles.headingTxt}>UPLOAD YOUR PROFILE PICTURE</Text>
                </View>
                <View style={styles.secondaryView}>
                    {this.state.image ? <View style={styles.profilePictureView}>
                        {this.state.indicator ? <ActivityIndicator color="#746761" size='large' /> : <Image source={{uri:this.state.image}} style={{height:220,width:220,borderRadius:110}} />}
                        <TouchableOpacity onPress={()=>this._selectExisting()} style={styles.interactView}>
                            <View style={styles.plusIconView}>
                                <Icon name='edit' type='MaterialIcons' style={styles.editIcon} />
                            </View>
                        </TouchableOpacity>
                    </View> : <View  style={styles.profilePictureView}>
                        {this.state.indicator ? <ActivityIndicator color="#746761" size='large' /> : null}
                        <TouchableOpacity onPress={()=>this._selectExisting()} style={styles.interactView}>
                            <View style={styles.plusIconView}>
                                <Icon name='plus' type=  'FontAwesome5' style={styles.icon} />
                            </View>
                        </TouchableOpacity>
                    </View> }
                </View>

                <View style={styles.selectPhotoView}>
                    <TouchableOpacity onPress={()=>this._takePhoto()} style={[styles.photoBtn, { marginBottom: 5 }]}>
                        <Text style={styles.selectPhotoTxt}>TAKE A PHOTO NOW</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this._selectExisting()} style={styles.photoBtn}>
                        <Text style={styles.selectPhotoTxt}>SELECT AN EXISTING PHOTO</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>Actions.AddNewWalk({profile:this.state.image})}>
                        <Text style={styles.linkTxt}>
                            skip this step
                        </Text>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity onPress={()=>Actions.AddNewWalk({profile:this.state.image})} disabled={this.state.image ? false : true} style={[styles.selectPhotoView, { position: 'absolute', bottom: 20,paddingVertical:15 }]}>
                    <View style={[styles.saveBtn,{backgroundColor:this.state.image ? '#B8CB21' : '#F0EEEF'}]}>
                        <Text style={styles.selectPhotoTxt}>SAVE AND CONTINUE</Text>
                    </View>
                </TouchableOpacity>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingHorizontal: 20,
    },
    headerView: {
        marginTop: 25,
        width: '100%'
    },
    headingTxt: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: '700',
        color: '#746764',
        fontFamily:'Lato-Bold'
    },
    secondaryView: {
        marginVertical: 30,
    },
    profilePictureView: {
        width: 250,
        height: 250,
        alignItems:'center',
        justifyContent:'center',
        borderRadius: 250 / 2,
        backgroundColor: '#FAFAFA',
    },
    interactView: {
        position: 'absolute',
        bottom: 0,
        width: '85%',
        alignItems: 'flex-end'
    },
    plusIconView: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#EEEEEE',
        elevation: 20
    },
    icon: {
        color: '#746764',
        fontSize: 22,
    },
    editIcon: {
        color: '#746764',
        fontSize: 25
    },
    selectPhotoView: {
        width: '90%',
        alignSelf: 'center',
    },
    photoBtn: {
        height: 40,
        backgroundColor: '#746761',
        width: '100%',
        justifyContent: 'center'
    },
    selectPhotoTxt: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily:'Lato-Bold'
    },
    linkTxt: {
        textAlign: 'center',
        fontSize: 12,
        paddingTop: 10,
        textDecorationLine: 'underline',
        fontFamily:'Lato-Regular'
    },
    saveBtn: {
        height: 40,
        backgroundColor: '#F0EEEF',
        width: '100%',
        justifyContent: 'center'
    },
});