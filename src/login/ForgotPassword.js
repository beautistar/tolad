import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  StatusBar,
  Keyboard
} from 'react-native';
import { Header, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Api from '../components/api'
import MessageBar from '../components/messageBar'

export default class ForgotPassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      isLoggedIn: false,
      message: '',
      checkEmail: false
    }
  }

  validEmail() {
    return (/^(?:[a-zA-Z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/.test(this.state.email))
  }

  check_email() {
    Keyboard.dismiss()

    if (!this.validEmail()) {
      this.setState({ checkEmail: true })
    } else {
      this.setState({ checkEmail: false })
      var apiData = new FormData()
      apiData.append("email", this.state.email)

      Api.post("/user/forgot_password", apiData)
        .then((result) => {
          if (result.result_code == 200) {
            this.setState({ message: result.message, color: 'green' }, () => this.messageBar._animateMessage())
            setTimeout(() => {
              Actions.pop()
            }, 3000)
          }
          else {
            this.setState({ message: result.message, color: 'red' }, () => this.messageBar._animateMessage())
          }
        })
    }
  }


  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Header style={{ width: '100%', backgroundColor: 'white' }} transparent >
          <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
            <Icon name='ios-arrow-back' type="Ionicons" style={{ color: '#7d716c', fontWeight: '100' }} />
          </TouchableOpacity>
          <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center' }} />
          <View style={{ width: '10%' }} />
        </Header>
        <StatusBar backgroundColor='white' barStyle='dark-content' />
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ alignItems: 'center', height: '100%' }} >
          <View style={styles.secondaryView}>
            <Text style={styles.welcome}>RESET PASSWORD</Text>
            <Text style={styles.descText}>For safety reasons you'll get an e-mail with a button to complete resetting your password</Text>
          </View>

          <View style={styles.txtInputView}>
            <TextInput
              style={[styles.txtInput, this.state.checkEmail ? { borderWidth: 2, borderColor: 'rgb(173,4,24)', backgroundColor: 'rgba(250,0,8,0.1)' } : { marginBottom: 10 }]}
              placeholder='username or email*'
              keyboardType='email-address'
              onChangeText={(text) => this.setState({ email: text })}
            />
            {this.state.checkEmail ? <View style={{ backgroundColor: 'rgb(173,4,24)', padding: 10, paddingHorizontal: 15 }}>
              <Text style={{ color: 'white', fontSize: 13 }}>Please enter vaild Email ID</Text>
            </View> : null}
          </View>

          <View style={styles.signupView}>
            <TouchableOpacity onPress={() => { this.check_email() }} style={[styles.signupBtn, { opacity: this.state.email ? 1 : 0.5 }]} disabled={this.state.email ? false : true}>
              <Text style={styles.signupTxt}>RESET PASSWORD</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.signupBtn, { marginTop: 10, backgroundColor: '#746761' }]}
              onPress={() => Actions.pop()}>
              <Text style={styles.signupTxt}>CANCEL</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <MessageBar ref={(ref) => this.messageBar = ref} message={this.state.message} color={this.state.color} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  secondaryView: {
    width: '85%',
    marginTop: 10
  },
  welcome: {
    fontSize: 15,
    color: 'black',
    paddingBottom: 10,
    fontFamily: 'Lato-Bold'
  },
  descText: {
    fontSize: 20,
    width: '80%',
    color: 'grey',
    fontFamily: 'Lato-Regular'
  },
  txtInputView: {
    marginVertical: 30,
    width: '85%',
  },
  txtInput: {
    paddingLeft: 13,
    height: 50,
    backgroundColor: '#F4F4F4',
    borderWidth: 0.5,
    borderColor: '#F4F4F4',
    fontSize: 15,
    fontFamily: 'Lato-Regular'
  },
  signupView: {
    width: '85%',
  },
  signupBtn: {
    height: 45,
    backgroundColor: '#B8CB21',
    justifyContent: 'center'
  },
  signupTxt: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 15,
    fontWeight: 'bold',
    fontFamily: 'Lato-Bold'
  },
  linkTxt: {
    textAlign: 'center',
    fontSize: 12,
    color: 'grey',
    textDecorationLine: 'underline',
    fontFamily: 'Lato-Regular'
  },
  socialLoginView: {
    width: '85%',
    marginTop: 10,
  },
  socialloginBtn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 45,
    borderWidth: 0.5,
    width: '100%',
    alignItems: 'center'
  },
  socialLoginTxt: {
    color: '#fff',
    fontSize: 12,
    fontWeight: 'bold',
    paddingLeft: 15,
    fontFamily: 'Lato-Bold'
  },
  icon: {
    width: 35,
    height: 35,
    tintColor: '#fff',
    marginRight: 25
  },
  googleIcon: {
    width: 30,
    height: 30,
    tintColor: '#fff',
    marginRight: 25
  }
});
