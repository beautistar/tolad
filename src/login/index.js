import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image,
  SafeAreaView,
  StatusBar,
  AsyncStorage,
  NativeModules,
  Keyboard,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import { Header, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Api from '../components/api'
import MessageBar from '../components/messageBar'
import { LoginManager, AccessToken } from "react-native-fbsdk";
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';

const { RNTwitterSignIn } = NativeModules

const Constants = {
  //Dev Parse keys
  TWITTER_COMSUMER_KEY: "3lmj4rqh9KMYcSz4pM7jLmLG2",
  TWITTER_CONSUMER_SECRET: "7YzrbMcPMubzk8zvXY6nMACHsAABHAdm3iGRDSRwPpDas7jIhx"
}

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      email: '',
      message: '',
      indicator: true,
      checkEmail: false,
      checkPassword: false
    }
  }

  componentDidMount() {
    this.setState({ indicator: false })
    GoogleSignin.configure({
      webClientId: '559029968346-prtmg96ijaagltc058b214ppmgbve3j2.apps.googleusercontent.com',
      iosClientId: '559029968346-1f0vm5cdr92cv7q4kt0tcj66nmn1b1l1.apps.googleusercontent.com',
    });
  }

  validPassword() {
    return (/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/.test(this.state.password))
  }
  validEmail() {
    return (/^(?:[a-zA-Z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/.test(this.state.username))
  }

  validate() {
    if (/^(?:[a-zA-Z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/.test(this.state.email)) {
      if (/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/.test(this.state.password)) {
        return true
      }
      else {
        return 'password'
      }

    } else {
      return 'email'
    }
  }
  
  _login() {
    Keyboard.dismiss()
    if (!this.state.username && !this.validPassword()) {
      this.setState({ checkEmail: true, checkPassword: true })
    }
    else if (this.state.username && !this.validPassword()) {
      this.setState({ checkEmail: false, checkPassword: true })
    }
    else if (!this.state.username && this.validPassword()) {
      this.setState({ checkEmail: true, checkPassword: false })
    }
    else {
      this.setState({ checkEmail: false, checkPassword: false })
      const apiData = new FormData()
      apiData.append('username', this.state.username)
      apiData.append('password', this.state.password)
      Api.post('/user/login', apiData)
        .then((result) => {
          if (result.result_code == 200) {
            AsyncStorage.setItem('userData', JSON.stringify(result.user_data))
            AsyncStorage.setItem('password', JSON.stringify(this.state.password))
            Actions.reset('Home', { profile: result.user_data.photo_url })
          }
          else {
            this.setState({ message: result.message, color: 'red' }, () => this.messageBar._animateMessage())
          }
        })
    }
  }

  _socialLogin() {
    const apiData = new FormData()
    apiData.append('username', this.state.username)
    apiData.append('password', this.state.password)
    Api.post('/user/login', apiData)
      .then((result) => {
        if (result.result_code == 200) {
          this.setState({ indicator: false })
          AsyncStorage.setItem('userData', JSON.stringify(result.user_data))
          AsyncStorage.setItem('password', JSON.stringify(this.state.password))
          Actions.reset('Home', { profile: result.user_data.photo_url })
        }
        else {
          this.setState({ message: result.message, color: 'red', indicator: false }, () => this.messageBar._animateMessage())
        }
      })
  }


  _signUp(type, profile) {
    var apiData = new FormData()
    apiData.append("login_type", type)
    apiData.append("username", this.state.username)
    apiData.append("email", this.state.email)
    apiData.append("password", this.state.password)
    apiData.append("photo_url", profile)

    Api.post("/user/register", apiData)
      .then((result) => {
        if (result.result_code == 200) {
          if (type == "email") {
            AsyncStorage.setItem('userData', JSON.stringify(result.user_data))
            Actions.UploadPhoto({ userId: result.id })
          }
          else {
            this.setState({ indicator: false })
            AsyncStorage.setItem('userData', JSON.stringify(result.user_data))
            Actions.AddNewWalk()
          }
        }
        else {
          if (type == "email") {
            this.setState({ message: result.message, color: 'red' }, () => this.messageBar._animateMessage())
          }
          else {
            this.setState({ indicator: true })
            this._socialLogin()
          }
        }
      })
  }



  _twitterSignIn() {
    RNTwitterSignIn.init(Constants.TWITTER_COMSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET)
    RNTwitterSignIn.logIn()
      .then(loginData => {
        const { authToken, authTokenSecret } = loginData
        if (authToken && authTokenSecret) {
          this.setState({ email: loginData.email, username: loginData.userName, password: loginData.userID }, () => this._signUp("twitter", `https://twitter.com/${loginData.userName}/profile_image?size=bigger`))
        }
      })
      .catch(error => {
        console.log('Twitter login error', error)
      }
      )
  }

  _facebookSignUp() {
    LoginManager.logOut()
    LoginManager.logInWithReadPermissions(['public_profile', 'email'])
      .then((result) => {
        console.log('res-->', result)
        if (result.isCancelled) {
          alert('Login cancelled');
        }
        else {
          AccessToken.getCurrentAccessToken().then((data) => {
            let token = data.accessToken;
            fetch('https://graph.facebook.com/v3.0/me?fields=email,name,picture&access_token=' + token)
              .then((resp) => resp.json())
              .then((result) => {
                this.setState({ email: result.email ? result.email : "", username: result.name, password: result.id }, () => this._signUp("facebook", result.picture.data.url))
              })
              .catch((error) => console.log('Facebook login error', error))
          })
        }
      })
  }

  _googleSignUp() {
    GoogleSignin.signOut()
    try {
      GoogleSignin.signIn()
        .then((result) => {
          this.setState({ email: result.user.email, username: result.user.name, password: result.user.id }, () => this._signUp("google", result.user.photo))
        })
    } catch (error) {
      console.log('error', error)
    }
  }

  render() {
    if (this.state.indicator) {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator color="#746761" size='large' />
        </View>
      )
    }
    else {
      return (
        <SafeAreaView style={styles.container}>
          <Header style={{ width: '100%', backgroundColor: 'white' }} transparent >
            <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
              <Icon name='ios-arrow-back' type="Ionicons" style={{ color: '#7d716c', fontWeight: '100' }} />
            </TouchableOpacity>
            <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center' }} />
            <View style={{ width: '10%' }} />
          </Header>
          <StatusBar backgroundColor='white' barStyle='dark-content' />
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ alignItems: 'center', paddingBottom: 20 }} >
            {/* <View style={{ width: '100%', alignItems: 'center', paddingHorizontal: 20 }}> */}
            <View style={styles.secondaryView}>
              {/* <Image source={require('../assets/images/appicon.png')} style={{ height: 100, width: 180 }} /> */}
              <Text style={styles.welcome}>WELCOME BACK!</Text>
              <Text style={styles.descText}>To login please provide your account details</Text>
            </View>

            <View style={styles.txtInputView}>

              <TextInput
                style={[styles.txtInput, this.state.checkEmail ? { borderWidth: 2, borderColor: 'rgb(173,4,24)', backgroundColor: 'rgba(250,0,8,0.1)' } : { marginBottom: 10 }]}
                placeholder='username or email*'
                keyboardType='email-address'
                onChangeText={(text) => this.setState({ username: text })}
              />
              {/* {this.state.checkEmail ?
                <View style={{ backgroundColor: 'rgb(173,4,24)', padding: 10, paddingHorizontal: 15 }}>
                  <Text style={{ color: 'white', fontSize: 13 }}>Please enter vaild Email ID</Text>
                </View>
                :
                null} */}

              <TextInput
                style={[styles.txtInput, this.state.checkPassword ? { borderWidth: 2, borderColor: 'rgb(173,4,24)', backgroundColor: 'rgba(250,0,8,0.1)' } : null]}
                placeholder='password*'
                secureTextEntry={true}
                onChangeText={(text) => this.setState({ password: text })}
              />
              {this.state.checkPassword ?
                <View style={{ backgroundColor: 'rgb(173,4,24)', padding: 10, paddingHorizontal: 15 }}>
                  <Text style={{ color: 'white', fontSize: 13 }}>Password should be atleast 8 digit and include alpha-numeric with one special character'</Text>
                </View>
                :
                null}

            </View>

            <View style={styles.signupView}>
              <TouchableOpacity style={[styles.signupBtn, { opacity: this.state.username && this.state.password ? 1 : 0.5 }]} disabled={this.state.username && this.state.password ? false : true}
                onPress={() => { this._login(), () => Keyboard.dismiss() }}>
                <Text style={styles.signupTxt}>SIGN IN</Text>
              </TouchableOpacity>
              <View style={[styles.signupBtn, { backgroundColor: 'transparent' }]}>
                <TouchableOpacity onPress={() => Actions.ForgotPassword()}><Text style={styles.linkTxt}>I forgot my password</Text></TouchableOpacity>
              </View>
            </View>

            <View style={{ width: Platform.OS == 'android' ? Dimensions.get('screen').width / 1.3 : Dimensions.get('screen').width / 1.2, marginVertical: 15 }}>
              <TouchableOpacity onPress={()=>Actions.Privacy()}>
                <Text style={{ fontFamily: 'Lato-Regular', fontSize: 12, textAlign: 'center' }}>
                  by signing up/in you agree to the terms of use and privacy policy from TOLAD which can be read and reviewed <Text style={{ fontFamily: 'Lato-Bold', textDecorationLine: 'underline', fontSize: 12 }}>here</Text>. Enjoy your walks and be safe!
              </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.socialLoginView}>
              <TouchableOpacity onPress={() => this._facebookSignUp()} style={[styles.socialloginBtn, { backgroundColor: '#465993', borderColor: '#465993', marginBottom: 5 }]}>
                <Text style={styles.socialLoginTxt}>SIGN IN USING FACEBOOK</Text>
                <View style={{ width: '20%', alignItems: 'center', paddingLeft: 30 }}>
                  <Image source={require('../assets/images/facebook.png')} style={styles.icon} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._twitterSignIn()} style={[styles.socialloginBtn, { backgroundColor: '#76A9EA', borderColor: '#76A9E9', marginBottom: 5 }]}>
                <Text style={styles.socialLoginTxt}>SIGN IN USING TWITTER</Text>
                <View style={{ width: '20%', alignItems: 'center', paddingLeft: 30 }}>
                  <Image source={require('../assets/images/twitter.png')} style={styles.icon} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._googleSignUp()} style={[styles.socialloginBtn, { backgroundColor: '#F34A37', borderColor: '#F34A37' }]}>
                <Text style={styles.socialLoginTxt}>SIGN IN USING GOOGLE+</Text>
                <View style={{ width: '20%', alignItems: 'center', paddingLeft: 30 }}>
                  <Image source={require('../assets/images/google.png')} style={styles.googleIcon} />
                </View>
              </TouchableOpacity>
            </View>
            {/* </View> */}
          </ScrollView>
          <MessageBar ref={(ref) => this.messageBar = ref} message={this.state.message} color={this.state.color} />

        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  secondaryView: {
    width: '85%',
    marginTop: 10
  },
  welcome: {
    fontSize: 15,
    color: 'black',
    paddingBottom: 10,
    fontFamily: 'Lato-Bold'
  },
  descText: {
    fontSize: 20,
    width: '80%',
    color: 'grey',
    fontFamily: 'Lato-Regular'
  },
  txtInputView: {
    marginVertical: 25,
    width: '85%',
  },
  txtInput: {
    paddingLeft: 13,
    height: 50,
    backgroundColor: '#F4F4F4',
    borderWidth: 0.5,
    borderColor: '#F4F4F4',
    fontSize: 15,
    fontFamily: 'Lato-Regular'
  },
  signupView: {
    width: '85%',
  },
  signupBtn: {
    height: 45,
    backgroundColor: '#B8CB21',
    justifyContent: 'center'
  },
  signupTxt: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 15,
    fontWeight: 'bold',
    fontFamily: 'Lato-Bold'
  },
  linkTxt: {
    textAlign: 'center',
    fontSize: 12,
    color: 'grey',
    textDecorationLine: 'underline',
    fontFamily: 'Lato-Regular'
  },
  socialLoginView: {
    width: '85%',
    marginTop: 10,
  },
  socialloginBtn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 45,
    borderWidth: 0.5,
    width: '100%',
    alignItems: 'center'
  },
  socialLoginTxt: {
    color: '#fff',
    fontSize: 12,
    fontWeight: 'bold',
    paddingLeft: 15,
    fontFamily: 'Lato-Bold'
  },
  icon: {
    width: 35,
    height: 35,
    tintColor: '#fff',
    marginRight: 25
  },
  googleIcon: {
    width: 30,
    height: 30,
    tintColor: '#fff',
    marginRight: 25
  }
});
