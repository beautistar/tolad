import React, { Component } from 'react';
import { BackHandler, Linking, Platform, PermissionsAndroid, Alert, NetInfo } from 'react-native';
import RNSettings from 'react-native-settings'
import { Scene, Router, Actions } from 'react-native-router-flux';
import Login from './login'
import SignUp from './signUp'
import AddNewWalk from './signUp/AddNewWalk'
import UploadPhoto from './signUp/UploadPhoto'
import Home from './home'
import AddPerson from './home/AddPerson'
import Map from './home/Map'
import AddBackground from './home/AddBackground'
import PersonalDetail from './home/PersonalDetail'
import ProfileImage from './home/ProfileImage'
import CropProfile from './home/CropProfile'
import WalkView from './home/WalkView'
import Settings from './home/Settings'
import ForgotPassword from './login/ForgotPassword'
import Contact from './home/Contact'
import Privacy from './home/Privacy'
import Help from './home/Help'
import PersonalProfile from './home/PersonalProfile'
import NoInternetConnection from './NetConnectionError'

export default class App extends Component {
  _getLocation() {
    if (Platform.OS == 'android') {
      PermissionsAndroid.request('android.permission.ACCESS_FINE_LOCATION')
        .then((res) => {
          if (res == 'granted') {
            navigator.geolocation.getCurrentPosition((position) => {
            },
              (error) => {
                console.log('Location Error', error)
                if (error.code == 2) {
                  Alert.alert(
                    'Location Service Disable',
                    'Please enable location service to use this App',
                    [
                      { text: 'OK', onPress: () => { this._openLocationSetting() } },
                    ],
                    { cancelable: false }
                  )
                }

              },
            )
            PermissionsAndroid.request('android.permission.WRITE_EXTERNAL_STORAGE')
          }
        })
        .catch(err => console.log('Permission Error', err))
    }
    else {
      navigator.geolocation.getCurrentPosition((position) => {
      },
        (error) => {
          if (error.code == 2) {
            Alert.alert(
              'Location Service Disable',
              'Please enable location service to use this App',
              [
                { text: 'OK', onPress: () => { this._openLocationSetting() } },
              ],
              { cancelable: false }
            )
          }
        },
      )
    }
  }
  _openLocationSetting() {
    if (Platform.OS == 'android') {
      RNSettings.openSetting(RNSettings.ACTION_LOCATION_SOURCE_SETTINGS).
        then((result) => {
          if (result === RNSettings.ENABLED) {
            console.log('location is enabled')
          }
        })
    }
    else {
      Linking.openURL("App-Prefs:root=Privacy&path=LOCATION_SERVICES")
    }
  }
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    this._getLocation()
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }

  componentWillUnmount() {
    this.backHandler.remove()
  }
  handleBackPress = () => {
    if (Actions.currentScene == 'Home' || Actions.currentScene == 'SignUp') {
      BackHandler.exitApp()
    }
  }
  handleFirstConnectivityChange(isConnected) {
    if (isConnected) {
      if (Actions.currentScene == "NoInternetConnection") {
        Actions.pop()
      }
    }
    else {
      Actions.NoInternetConnection()
    }
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }
  render() {
    return (
      <Router>
        <Scene key="root" hideNavBar>
          <Scene key="Login" component={Login} />
          <Scene key="ForgotPassword" component={ForgotPassword} />
          <Scene key="SignUp" component={SignUp} initial />
          <Scene key="AddNewWalk" component={AddNewWalk} />
          <Scene key="UploadPhoto" component={UploadPhoto} />
          <Scene key="Home" component={Home} panHandlers={null} />
          <Scene key="AddPerson" component={AddPerson} />
          <Scene key="Map" component={Map} />
          <Scene key="AddBackground" component={AddBackground} />
          <Scene key="PersonalDetail" component={PersonalDetail} />
          <Scene key="ProfileImage" component={ProfileImage} />
          <Scene key="CropProfile" component={CropProfile} />
          <Scene key="WalkView" component={WalkView} />
          <Scene key="Settings" component={Settings} />
          <Scene key="Contact" component={Contact} />
          <Scene key="Privacy" component={Privacy} />
          <Scene key="Help" component={Help} />
          <Scene key="PersonalProfile" component={PersonalProfile} />
          <Scene key="NoInternetConnection" component={NoInternetConnection} />
        </Scene>
      </Router>
    );
  }
}


